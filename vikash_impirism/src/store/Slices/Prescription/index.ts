import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = {
  prescription: [],
  
};

const Prescription = createSlice({
  name: "PrescriptionSlice",
  initialState,
  reducers: {
    setPrescriptionHistory(state: typeof initialState, action: PayloadAction<any>) {
      state.prescription = action.payload;
    },
  },
});
export default Prescription;

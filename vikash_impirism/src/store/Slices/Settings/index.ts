import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { User } from "../../../models/User";

const initialState: {
    prescribers: User[]
} = {
    prescribers: [],
}

const Settings = createSlice({
name: "SettingsSlice",
initialState,
reducers: {
    setPrescriptionData(state: typeof initialState,action: PayloadAction<{data: User[]}>){
       state.prescribers = action.payload.data
    }
}
});

export default Settings;
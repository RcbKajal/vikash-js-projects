import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { PatientInterface } from "../../../interfaces/patient";

const initialState: {
  patients: PatientInterface[]
} = {
  patients: [],
};

const Patient = createSlice({
  name: "PatientSlice",
  initialState,
  reducers: {
    setCurrentPatient(state: typeof initialState, action: PayloadAction<any>) {
      state.patients = action.payload;
    },
  },
});
export default Patient;

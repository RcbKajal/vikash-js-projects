import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState: any = {
  products: null
};

const Product = createSlice({
  name: "ProductSlice",
  initialState,
  reducers: {
    setProductCatalogData(state: typeof initialState, action: PayloadAction<any>) {
      state.products = action.payload.data;
    },
  },
});
export default Product;

export interface PrescriptionInterface {
    Rx_Number: string,
    Order_Number: string;
    Patient_Name: string;
    Prescriber_Name: string;
    Status: string;
    Tracking: string;
    Medication: string;
    Issue_Date: string;
    Prescription_Number: string;
    Product_Name: string;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
}
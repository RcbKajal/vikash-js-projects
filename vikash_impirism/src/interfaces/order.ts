export interface OrderInterface {
    Rx_Number: string,
    Order_Number: string;
    Patient_Name: string;
    Prescriber_Name: string;
    Status: string;
    Tracking: string;
    Medication: string;
    Issue_Date: string;
    fname: string;
    lname: string;
}
export interface PatientInterface {
    firstName: string,
    lastName: string;
    dateOfBirth: string;
    phoneNumber: string;
    patId: string;
    fname: string;
    lname: string;
    area_code: string;
    phone_no: string;
    email: string;
    birth_date: string;
    addr1: string;
    addr2: string;
    state_cd: string;
    zip: string;
    gender_cd: string;
}
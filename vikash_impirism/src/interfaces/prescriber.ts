export interface PrescriberInterface {
    first_name: string;
    last_name: string;
    mobile: string;
    email: string;
    notification: string;
    npi: string;
    Specialty: string;
    profile_image_path: string;
    doctor_id: string
}
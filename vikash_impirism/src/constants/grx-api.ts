import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

const {REACT_APP_GRX_BASE_URL} = process.env;
console.log(REACT_APP_GRX_BASE_URL,'REACT_APP_GRX_BASE_URL');

const Doctor = axios.create({
  baseURL: REACT_APP_GRX_BASE_URL,
  timeout: 25000,
  headers: {
    "Content-Type": "application/json",
  },
});

Doctor.interceptors.request.use((config: AxiosRequestConfig) => {

  // spinning start to show
  // UPDATE: Add this code to show global loading indicator
  document.body.classList.add('loading-indicator');

  // const token = window.localStorage.token;
  // if (token) {
  //    config.headers.Authorization = `token ${token}`
  // }
  return config
}, (error: string) => {
  document.body.classList.remove('loading-indicator');
  return error;
});

Doctor.interceptors.response.use((response:AxiosResponse) => {

  // spinning hide
  // UPDATE: Add this code to hide global loading indicator
  document.body.classList.remove('loading-indicator');

  return response;
}, (error: AxiosError) => {
  document.body.classList.remove('loading-indicator');
  return error.response;
});

export default Doctor;

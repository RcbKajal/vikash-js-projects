//auth--->
export const REGISTER_STAFF = "/doctorapi/adddoctor?type=S";
export const REGISTER_DOCTOR = "/doctorapi/adddoctor?type=D";
export const FORGOT_PASSWORD = "/doctorapi/forgotpasswordv1";
export const RESET_PASSWORD = "/doctorapi/changepassword";
export const EMAIL_LOGIN = "/doctorapi/login";
export const MOBILE_LOGIN = "/doctorapi/mobilelogin";

//dashboard-->
export const GET_STATISTICS = "/doctor-portal/dashboard/get-stats";
export const GET_RECENT_ORDERS = "/doctor-portal/order/rx-by-md-id-top-five";
export const PRODUCT_SPOTLIGHT = "/productapi/getproducts";
export const GET_ALLERGIES = "/constantapi/getconstantsv1";
// https://mobileauth.imprimisrx.com/development/webservices/1.0.8/index.php/constantapi/getconstantsv1?api_key=9e969f89f4e42d8eb3ef56582c7a69c6&constant_id=1

//used for all pages-->
export const GET_SPECIALTIES = "/constantapi/getspecialities";


//patient --->
export const GET_CURRENT_PATIENT = "/doctor-portal/patient/all";
export const GET_SINGLE_PATIENT = "/doctor-portal/patient/single";
export const NEW_PATIENT = "/doctor-portal/patient/add";


//prescription --->
export const GET_PRESCRIPTION_HISTORY = "/doctor-portal/order/rx-by-md-id";
export const GET_ORDER_DETAIL = "/doctor-portal/order/rx-calc-order-price-for-invoice";


//setting --> 
//user
export const PERSONAL_USER_PROFILE = "https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updatedoctor?type=D";
export const LOGIN_USER_PROFILE = "https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updatedoctor?type=D";


//prescriber
export const PRESCRIBER_PROFILE_GET  = "/doctorstaffapi/getstaffdoctors";
// export const PRESCRIBER_PROFILE_UPDATE  = "/doctorstaffapi/updatestaffdoctor";
export const PRESCRIBER_PROFILE_UPDATE  = "/doctorapi/updatedoctor";

//setting prescriber popup
export const ADD_NEW_PRESCRIBER = "/doctorstaffapi/addstaffdoctor";

export const REMOVE_NEW_PRESCRIBER = "/doctorstaffapi/removestaffdoctor";
export const ADD_NEW_SHIPPING_ADDRESS = "https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updatedoctor?type=D";
export const ADD_NEW_CARD = "https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updatedoctor?type=D";


//product-->
export const PRODUCT_CATALOG = "https://mobileauth.imprimisrx.com/development/webservices/1.0.8/index.php/productapi/getproductsv1";
export const PRODUCT_Preservative_FILTER = "/constantapi/getconstantsv1";
export const PRODUCT_Dosage_FILTER = "https://mobileauth.imprimisrx.com/development/webservices/1.0.8/index.php/constantapi/getdosageforms";
export const PRODUCT_CATALOG_CAT_FILTER = "/packageapi/getpackagesv1";
export const ADD_FAV_PRODUCT = "/doctorfavouriteproductapi/addfavouriteproduct";
export const REMOVE_FAV_PRODUCT = "/doctorfavouriteproductapi/removefavouriteproduct";


//used for images -->
export const LOCAL_PROFILE_ENV = "https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updatedoctor?type=D";
export const ASSET_BASE_URL = "https://mobileauth.imprimisrx.com/development/webservices/images/originals";


export const PROFILE = "user";
export const UPDATE = "auth";

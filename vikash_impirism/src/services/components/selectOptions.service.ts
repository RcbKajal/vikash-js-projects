export const notificationOptions = [
  { label: "Email", value: "email" },
  { label: "SMS", value: "sms" },
  { label: "Both Email and SMS", value: "both" },
];
export const genderOptions = [
  { label: "Male", value: "m" },
  { label: "Female", value: "f" },
  { label: "Other", value: "o" },
];

export const specialityOptions = [];

export const cityOptions = [
  { label: "London", value: "london" },
  { label: "Delhi", value: "delhi" },
  { label: "Kolkata", value: "kolkata" },
];
export const stateOptions = [
  { label: "Punjab", value: "punjab" },
  { label: "Himachal", value: "himachal" },
  { label: "Jammu", value: "jammu" },
];

import { FormBuilder, Validators } from "react-reactive-form";

import { AxiosResponse } from "axios";
import { EMAIL_LOGIN } from "../../constants/Endpoints";
import PhysicianDoctor from "../../constants/api";
import { User } from "../../models/User";
import { toast } from "react-toastify";

export default class AuthService {

  loginForm = FormBuilder.group({
    email: ["", [Validators.required, Validators.email]],
    password: ["", Validators.required],
    rememberMe: [false],
  });

  constructor() {
    if (localStorage.getItem('login_username') && localStorage.getItem('login_password')) {
      this.loginForm = FormBuilder.group({
        email: [localStorage.getItem('login_username'), [Validators.required, Validators.email]],
        password: [localStorage.getItem('login_password'), Validators.required],
        rememberMe: [true],
      });

    }
  }

  loginHandler = async () => {

    try {
      this.loginForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
      this.loginForm.controls.password.markAsTouched({ emitEvent: true, onlySelf: true });

      const data = this.loginForm.value;
      if (this.loginForm.invalid) {
        return false;
      }

      const res: AxiosResponse = await PhysicianDoctor.post(EMAIL_LOGIN, {
        username: data.email,
        password: data.password,
      });

      if (res.data.status === "OK") {
        sessionStorage.setItem("isSessionOngoing", "true");
        if (data.rememberMe === true) {
          localStorage.setItem("rememberMe", "true");
          localStorage.setItem("login_username", data.email);
          localStorage.setItem("login_password", data.password);
        } else {
          localStorage.removeItem("rememberMe");
          localStorage.removeItem("login_username");
          localStorage.removeItem("login_password");
        }
        localStorage.setItem("api_key", res.data.response.api_key);
        localStorage.setItem("isLoggedIn", "true");
        return User.create(res.data.response, true);
      } else {
        toast(res.data.message);
        return false;
      }

    } catch (err: any) {
      if (err?.response?.data?.status === "Error") {
        toast(err?.response.data.message);
        return false;
      }
    };
  };
}

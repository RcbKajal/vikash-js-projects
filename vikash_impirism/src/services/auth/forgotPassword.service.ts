import { FormBuilder, Validators } from 'react-reactive-form';

import { AxiosResponse } from 'axios';
import { FORGOT_PASSWORD } from '../../constants/Endpoints';
import { NavigateFunction } from 'react-router-dom';
import PhysicianDoctor from '../../constants/api';
import { toast } from 'react-toastify';

export const forgotPswForm = FormBuilder.group({
  email: ['', [Validators.required, Validators.email]]
});

export const forgotPasswordHandler = async (router: NavigateFunction) => {
  try {
    forgotPswForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
    if (forgotPswForm.invalid) {
      return;
    }
    const data = forgotPswForm.value;
    const res: AxiosResponse = await PhysicianDoctor.post(FORGOT_PASSWORD, {
      email: data.email
    });
    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }
    if (res.data.status === 'OK') {
      if (typeof window !== 'undefined') {
        localStorage.setItem('forget-email', data.email);
      }
      // toast('Kindly check your email to reset your password.')
      router('/forgot-password/done');
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

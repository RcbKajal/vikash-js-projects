import { FormBuilder, Validators } from 'react-reactive-form';
import { REGISTER_DOCTOR, REGISTER_STAFF } from '../../constants/Endpoints';

import { AxiosResponse } from 'axios';
import { NavigateFunction } from 'react-router-dom';
import PhysicianDoctor from '../../constants/api';
import { toast } from 'react-toastify';

export const staffSignUpForm = FormBuilder.group({
  firstName: ['', [Validators.required]],
  lastName: ['', [Validators.required]],
  phone: ['', [Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  password: ['', [Validators.required]],
  npi: ['', [Validators.required]],
  donot_npi: [false],
  street: ['', [Validators.required]],
  apt_suite: [''],
  city: ['', [Validators.required]],
  state: ['', [Validators.required]],
  code: ['', [Validators.required]],
  staff_firstName: ['', [Validators.required]],
  staff_lastName: ['', [Validators.required]],
  staff_phone: ['', [Validators.required]],
  staff_email: ['', [Validators.required]],
  rememberMe: [true, []]
});

export const staffSignUpHandler = async (router: NavigateFunction) => {
  try {
    staffSignUpForm.controls.firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.phone.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.password.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.npi.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.street.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.apt_suite.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.city.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.state.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.code.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.staff_firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.staff_lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.staff_phone.markAsTouched({ emitEvent: true, onlySelf: true });
    staffSignUpForm.controls.staff_email.markAsTouched({ emitEvent: true, onlySelf: true });

    if (staffSignUpForm.invalid) {
      return;
    }
    // if((!staffSignUpForm.value.donot_npi && !staffSignUpForm.value.npi)) {
    //   return;
    // }

    let data = {
      first_name: staffSignUpForm.value.staff_firstName,
      last_name: staffSignUpForm.value.staff_lastName,
      mobile: staffSignUpForm.value.staff_phone,
      email: staffSignUpForm.value.staff_email,
      password: staffSignUpForm.value.password,
      npi: staffSignUpForm.value.npi,
      street: staffSignUpForm.value.street,
      apt_suite: staffSignUpForm.value.apt_suite,
      city: staffSignUpForm.value.city,
      state: staffSignUpForm.value.state,
      postal_code: staffSignUpForm.value.code,
      country: 'US',
      pres_fname: staffSignUpForm.value.firstName,
      pres_lname: staffSignUpForm.value.lastName,
      pres_email: staffSignUpForm.value.email
    };
    const res: AxiosResponse = await PhysicianDoctor.post(REGISTER_STAFF, data);

    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }

    if (res.data.status === 'OK') {
      toast('Registered Successfully');
      router('/home/dashboard');
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

export const physicianSignUpForm = FormBuilder.group({
  firstName: ['', [Validators.required]],
  lastName: ['', [Validators.required]],
  phone: ['', [Validators.required]],
  notification_preference: ['', [Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  password: ['', [Validators.required]],
  npi: ['', [Validators.required]],
  donot_npi: [false],
  specialty: ['', [Validators.required]],
  street: ['', [Validators.required]],
  apt_suite: [''],
  city: ['', [Validators.required]],
  state: ['', [Validators.required]],
  code: ['', [Validators.required]],
  rememberMe: [true, []]
});

export const physicianSignUpHandler = async (router: NavigateFunction) => {
  try {
    physicianSignUpForm.controls.firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.phone.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.notification_preference.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.password.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.npi.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.specialty.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.street.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.apt_suite.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.city.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.state.markAsTouched({ emitEvent: true, onlySelf: true });
    physicianSignUpForm.controls.code.markAsTouched({ emitEvent: true, onlySelf: true });

    if (physicianSignUpForm.invalid) {
      return;
    }
    // if((!physicianSignUpForm.value.donot_npi && !physicianSignUpForm.value.npi)) {
    //   return;
    // }
    let data = {
      first_name: physicianSignUpForm.value.firstName,
      last_name: physicianSignUpForm.value.lastName,
      mobile: physicianSignUpForm.value.phone,
      email: physicianSignUpForm.value.email,
      password: physicianSignUpForm.value.password,
      specialty: physicianSignUpForm.value.specialty,
      street: physicianSignUpForm.value.street,
      city: physicianSignUpForm.value.city,
      state: physicianSignUpForm.value.state,
      postal_code: physicianSignUpForm.value.code,
      country: 'US',
      notification_preference: physicianSignUpForm.value.notification_preference,
      npi: physicianSignUpForm.value.npi,
      apt_suite: physicianSignUpForm.value.apt_suite
    };
    const res: AxiosResponse = await PhysicianDoctor.post(REGISTER_DOCTOR, data);
    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }

    if (res.data.status === 'OK') {
      toast('Registered Successfully');
      router('/home');
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

import { FormBuilder, Validators } from 'react-reactive-form';
import { LOGIN_USER_PROFILE, PERSONAL_USER_PROFILE } from '../../../constants/Endpoints';

import { AxiosResponse } from 'axios';
import Doctor from '../../../constants/grx-api';
import { User } from '../../../models/User';
import { toast } from 'react-toastify';

export const personalLoginForm = FormBuilder.group({
  username: ['', [Validators.required]],
  password: ['', [Validators.required]]
});

export const SettingLoginHandler = async (user: User) => {
  try {
    personalLoginForm.controls.username.markAsTouched({ emitEvent: true, onlySelf: true });
    personalLoginForm.controls.password.markAsTouched({ emitEvent: true, onlySelf: true });

    if (personalLoginForm.invalid) {
      return;
    }
    
    let data = {
      username: personalLoginForm.value.username,
      password: personalLoginForm.value.password,
      doctor_id: user.doctor_id
    };

    const res: AxiosResponse = await Doctor.post(PERSONAL_USER_PROFILE, data, { params: { api_key: localStorage.getItem('api_key') } });
    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }
    if (res.data.status === 'OK') {
      toast('Profile Update Successfully');
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

export const personalUserForm = FormBuilder.group({
  firstName: ['', [Validators.required]],
  lastName: ['', [Validators.required]],
  phone: ['', [Validators.required]],
  email: ['', [Validators.required]],
  doctor_id: ['', [Validators.required]]
});

export const SettingUserHandler = async (user: User) => {
  try {

    personalUserForm.controls.firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    personalUserForm.controls.lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    personalUserForm.controls.phone.markAsTouched({ emitEvent: true, onlySelf: true });
    personalUserForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });

    if (personalUserForm.invalid) {
      return;
    }

    let data = {
      first_name: personalUserForm.value.firstName,
      last_name: personalUserForm.value.lastName,
      mobile: personalUserForm.value.phone,
      email: personalUserForm.value.email,
      doctor_id: user.doctor_id
    };

    const res: AxiosResponse = await Doctor.post(LOGIN_USER_PROFILE, data, { params: { api_key: localStorage.getItem('api_key') } });
    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }

    if (res.data.status === 'OK') {
      toast('Profile Update Successfully');
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

import { FormBuilder, Validators } from 'react-reactive-form';

import { ADD_NEW_CARD } from '../../../constants/Endpoints';
import { AxiosResponse } from 'axios';
import Doctor from '../../../constants/grx-api';
import { NavigateFunction } from 'react-router-dom';
import React from 'react';
import { toast } from 'react-toastify';

export const addNewCardForm = FormBuilder.group({
  cc_number: ['', [Validators.required]],
  exp_date: ['', [Validators.required]],
  cvc: ['', [Validators.required]],
  cardholdername: ['', [Validators.required]],
  billingZc: ['', [Validators.required]]
});

export const AddNewCardFormHandler = async (event: React.SyntheticEvent, router: NavigateFunction) => {
  try {
    event.preventDefault();
    addNewCardForm.controls.cc_number.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewCardForm.controls.exp_date.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewCardForm.controls.cvc.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewCardForm.controls.cardholdername.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewCardForm.controls.billingZc.markAsTouched({ emitEvent: true, onlySelf: true });

    if (addNewCardForm.invalid) {
      return;
    }
    let data = {
      cc_number: addNewCardForm.value.cc_number,
      exp_date: addNewCardForm.value.exp_date,
      cvc: addNewCardForm.value.cvc,
      cardholdername: addNewCardForm.value.cardholdername,
      billingZc: addNewCardForm.value.billingZc
    };
    const res: AxiosResponse = await Doctor.post(ADD_NEW_CARD, data);
    if (res.data.status === 'Error') {
      return toast(res.data.message);
    }
    if (res.data.status === 'OK') {
      toast('Add New Prescriber Registered Successfully');
      // router.back();
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

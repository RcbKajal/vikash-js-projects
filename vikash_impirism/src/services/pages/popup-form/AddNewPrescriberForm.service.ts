import { FormBuilder, Validators } from "react-reactive-form";

import { ADD_NEW_PRESCRIBER } from "../../../constants/Endpoints";
import { AxiosResponse } from "axios";
import { NavigateFunction } from "react-router-dom";
import PhysicianDoctor from "../../../constants/api";
import { User } from "../../../models/User";
import { toast } from "react-toastify";

export let addNewPrescriberForm = FormBuilder.group({
  firstName: ['', [Validators.required]],
  lastName: ['', [Validators.required]],
  phone: ['', [Validators.required]],
  npi: ['', [Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  donot_npi: [false],
});

export const AddNewPrescriberFormHandler = async (router: NavigateFunction, props: {handleClose: (e: string) => void}, userData: User) => {
  try{
    addNewPrescriberForm.controls.firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewPrescriberForm.controls.lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewPrescriberForm.controls.phone.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewPrescriberForm.controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
    addNewPrescriberForm.controls.npi.markAsTouched({ emitEvent: true, onlySelf: true });
  
    if (!addNewPrescriberForm.value.firstName && !addNewPrescriberForm.value.lastName && !addNewPrescriberForm.value.phone  && !addNewPrescriberForm.value.phone)  {
      return;
    }
  
    if((!addNewPrescriberForm.value.donot_npi && !addNewPrescriberForm.value.npi)) {
      return;
    }
  
    let data = {
      firstName: addNewPrescriberForm.value.firstName,
      lastName: addNewPrescriberForm.value.lastName,
      phone: addNewPrescriberForm.value.phone,
      email: addNewPrescriberForm.value.email,
      npi: addNewPrescriberForm.value.npi,
      doctor_id: userData.doctor_id
    };
  
    const res: AxiosResponse = await PhysicianDoctor.post(ADD_NEW_PRESCRIBER, data, {params: {api_key: localStorage.getItem('api_key')}});
        // console.log(res,'prscriber new');   
        if (res?.data?.status === "Error") {
          return toast(res.data.message);
        }
  
        if (res?.data?.status === "OK") {
          addNewPrescriberForm.reset();
          toast('Add New Prescriber Registered Successfully');
          props.handleClose('New Prescriber Added');
        }
  }catch(err:any){
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }

};

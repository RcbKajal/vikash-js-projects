import { FormBuilder, Validators } from 'react-reactive-form';

import { AxiosResponse } from 'axios';
import Doctor from '../../../constants/grx-api';
import { NEW_PATIENT } from '../../../constants/Endpoints';
import { User } from '../../../models/User';
import { toast } from 'react-toastify';

export const CreateNewPatientForm = FormBuilder.group({
  firstName: ['', [Validators.required]],
  lastName: ['', [Validators.required]],
  gender: ['', [Validators.required]],
  dob: ['', [Validators.required]],
  email: ['', [Validators.required, Validators.email]],
  phone: ['', [Validators.required]],
  isAllergies: ['true'],
  allergies: ['', [Validators.required]],
  cMedications: ['', [Validators.required]],
  pCondition: ['', [Validators.required]],
  street: ['', [Validators.required]],
  apt_suite: [''],
  city: ['', [Validators.required]],
  state: ['', [Validators.required]],
  code: ['', [Validators.required]],
  isPaymentMethod: ['true'],
  cardholderName: ['', [Validators.required]],
  cardNumber: ['', [Validators.required]],
  expiry: ['', [Validators.required]],
  cvv: ['', [Validators.required]],
  zip_code: ['', [Validators.required]],
  country: ['US', [Validators.required]]
});

export const CreatePatientHandler = async (
  user: User,
  props: {
    handleClose: () => void;
  }
) => {
  try {
    const controls = CreateNewPatientForm.controls;
    controls.firstName.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.lastName.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.gender.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.dob.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.email.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.phone.markAsTouched({ emitEvent: true, onlySelf: true });

    controls.street.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.apt_suite.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.city.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.state.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.code.markAsTouched({ emitEvent: true, onlySelf: true });

    controls.cardholderName.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.cardNumber.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.expiry.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.cvv.markAsTouched({ emitEvent: true, onlySelf: true });
    controls.zip_code.markAsTouched({ emitEvent: true, onlySelf: true });

    controls.allergies.markAsTouched({ emitEvent: true, onlySelf: true });

    if (controls.firstName.invalid || controls.lastName.invalid || controls.gender.invalid || controls.dob.invalid || controls.email.invalid || controls.phone.invalid) {
      return;
    }

    if (controls.street.invalid || controls.apt_suite.invalid || controls.city.invalid || controls.state.invalid || controls.code.invalid) {
      return;
    }

    // eslint-disable-next-line eqeqeq
    if (controls.isAllergies.value == 'true' && controls.allergies.invalid) {
      return;
    }

    // eslint-disable-next-line eqeqeq
    if (controls.isPaymentMethod.value == 'true' && (controls.cardholderName.invalid || controls.cardNumber.invalid || controls.expiry.invalid || controls.cvv.invalid || controls.zip_code.invalid)) {
      return;
    }

    let data = {
      first_name: CreateNewPatientForm.value.firstName,
      last_name: CreateNewPatientForm.value.lastName,
      gender: CreateNewPatientForm.value.gender,
      dob: CreateNewPatientForm.value.dob,
      email: CreateNewPatientForm.value.email,
      mobile: CreateNewPatientForm.value.phone,
      isAllergies: CreateNewPatientForm.value.isAllergies,
      allergies: CreateNewPatientForm.value.allergies,
      cMedications: CreateNewPatientForm.value.cMedications,
      pCondition: CreateNewPatientForm.value.pCondition,
      street: CreateNewPatientForm.value.street,
      apt_suite: CreateNewPatientForm.value.apt_suite,
      city: CreateNewPatientForm.value.city,
      state: CreateNewPatientForm.value.state,
      code: CreateNewPatientForm.value.code,
      country: CreateNewPatientForm.value.country,
      paymentMethod: CreateNewPatientForm.value.isPaymentMethod,
      cardholderName: CreateNewPatientForm.value.cardholderName,
      cardNumber: CreateNewPatientForm.value.cardNumber,
      expiry: CreateNewPatientForm.value.expiry,
      cvv: CreateNewPatientForm.value.cvv,
      zip_code: CreateNewPatientForm.value.zip_code,
      user: user.doctor_id
    };

    const res: AxiosResponse = await Doctor.post(NEW_PATIENT, data);
    if (res.data.statusCode === 420) {
      return toast(res.data.message);
    }
    if (res.data.status === 'OK') {
      CreateNewPatientForm.reset();
      toast('Patient created successfully.');
      props.handleClose();
    }
  } catch (err: any) {
    if (err?.response?.data?.status === 'Error') {
      toast(err?.response.data.message);
      return;
    }
  }
};

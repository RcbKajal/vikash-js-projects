import { FormBuilder, Validators } from "react-reactive-form";

import { Dispatch } from "react";
import { dashboardActions } from "../../../store/Actions";
import { useNavigate } from "react-router-dom";

export const AddMedicationForm = FormBuilder.group({
  medication: ['', [Validators.required]],
  size: ['', [Validators.required]],
  quantity: ['', [Validators.required]],
  supply: ['', [Validators.required]],
  refills: ['', [Validators.required]],
  choose_dosing_instruction: ['', [Validators.required]],
  choose_medical_necessity: ["", [Validators.required]],
  comments: ["", [Validators.required]],
});

export const AddMedicationHandler = async (props:{
  handleClose: () => void
}, dispatch: Dispatch<any>) => {
  const router = useNavigate();
  AddMedicationForm.controls.medication.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.quantity.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.supply.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.refills.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.choose_dosing_instruction.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.choose_medical_necessity.markAsTouched({ emitEvent: true, onlySelf: true })
  AddMedicationForm.controls.comments.markAsTouched({ emitEvent: true, onlySelf: true })
 
  if (AddMedicationForm.invalid) {
    return;
  }

  let data = {
    medication: AddMedicationForm.value.medication,
    quantity: AddMedicationForm.value.quantity,
    supply: AddMedicationForm.value.supply,
    refills: AddMedicationForm.value.refills,
    choose_dosing_instruction: AddMedicationForm.value.choose_dosing_instruction,
    choose_medical_necessity: AddMedicationForm.value.choose_medical_necessity,
    comments: AddMedicationForm.value.comments,
  };

  dispatch(dashboardActions.setMedicationData({data:data}));
  AddMedicationForm.reset();
  props.handleClose();
  router("/home/prescription-order", );
};

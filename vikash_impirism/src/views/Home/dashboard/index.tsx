import { Box, Button, Container, Stack, Typography } from '@mui/material';
import { GET_RECENT_ORDERS, GET_STATISTICS, PRODUCT_SPOTLIGHT } from '../../../constants/Endpoints';
import { NavLink, useNavigate } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store';

import { AddMedication } from '../../../components/popup/AddMedication';
import { AddNewPatient } from '../../../components/popup/AddNewPatient';
import { AxiosError } from '../../../interfaces/api';
import { AxiosResponse } from 'axios';
import { BootstrapDialog } from '../../../core/tables/tableStyles';
import Carousel from 'react-material-ui-carousel';
import Chat from '../../../assets/icons/chat.svg';
import Doctor from '../../../constants/grx-api';
import Header from '../../../components/header/header';
import Left from '../../../assets/icons/left_arrow.svg';
import { OrderTable } from '../../../components/tables/OrderTable';
import PhysicianDoctor from '../../../constants/api';
import { Product } from '../../../models/Product';
import Right from '../../../assets/icons/right_arrow.svg';
import SecondaryButton from '../../../core/buttons/secondary-button';
import { User } from '../../../models/User';
import { dashboardActions } from '../../../store/Actions';
import { isLoggedIn } from '../../../services/auth/auth.service';
import { toast } from 'react-toastify';

export default function DashboardPage() {
  const router = useNavigate();
  const dispatch = useAppDispatch();

  const [isLoaded, setIsLoaded] = useState(false);
  const [medication, setMedication] = useState(false);
  const [open, setOpen] = React.useState(false);

  const productData = useAppSelector((state) => state.dashboardReducer.data);
  const statisticsData = useAppSelector((state) => state.dashboardReducer.statistics);
  const orderData = useAppSelector((state) => state.dashboardReducer.orders);
  const user: User = useAppSelector((state) => state.profileReducer.user);

  useEffect(() => {
    if (!isLoggedIn()) {
      router('/');
    } else {
      setIsLoaded(true);
    }
  }, [router]);

  useEffect(() => {
    if (user) {
      getStatistics();
      getRecentOrders();
      getProductSpotlight();
    }
  }, [dispatch, user]);

  // products
  const getProductSpotlight = async () => {
    try {
      const res: AxiosResponse = await PhysicianDoctor.post(PRODUCT_SPOTLIGHT, {}, { params: { api_key: localStorage.getItem('api_key'), is_spotlight: 1 } });
      const productData = Product.createFromArray(res.data.response);
      dispatch(dashboardActions.setProductData({ data: productData }));
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };

  // statics
  const getStatistics = async () => {
    try {
      const res: AxiosResponse = await Doctor.get(GET_STATISTICS, { params: { user_id: user?.doctor_id } });
      const data = {
        patients: res.data.patients,
        prescriptions: res.data.prescriptions
      };

      dispatch(dashboardActions.setStatisticsData(data));
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };

  // recent orders
  const getRecentOrders = async () => {
    try {
      const res: AxiosResponse = await Doctor.get(GET_RECENT_ORDERS, { params: { md_id: user?.md_id } });
      dispatch(dashboardActions.setRecentOrders(res.data));
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleMedication = () => {
    setMedication(true);
  };

  const handleNavigation = () => {
    router('/home/patient');
  };

  const handleViewPage = () => {
    router('/home/prescriptions');
  };

  const handleClose = () => {
    setOpen(false);
    setMedication(false);
  };

  return (
    (isLoaded &&
    user) ? (
      <>
        {open && (
          <BootstrapDialog onClose={handleClose} open={open} PaperProps={{ style: { minHeight: '84%', maxHeight: '89%', minWidth: '75%', maxWidth: 1298 } }}>
            <AddNewPatient handleClose={handleClose} />
          </BootstrapDialog>
        )}
        {medication && (
          <BootstrapDialog onClose={handleClose} open={medication} PaperProps={{ style: { minHeight: '84%', maxHeight: '89%', minWidth: '40%', maxWidth: 780 } }}>
            <AddMedication handleClose={handleClose} />
          </BootstrapDialog>
        )}
        <Stack component="main" className="default-layout">
          <Header />
          <Box className="dashboard-page" position="relative">
            <Container maxWidth="xl">
              <Stack className="main-content-wrap">
                <Stack id="dashboard_set" direction={{ xs: 'column', sm: 'column', lg: 'row', xl: 'row' }}>
                  <Box flex={{ xs: 1 }} className="dashboard_col_layout">
                    <Stack direction={{ xs: 'column', md: 'row' }} className="dashboardFlex">
                      <Box padding={1} flex={{ xs: 1, md: 3 }}>
                        <Box className="card-sample-block">
                          {user.isApproved && (
                            <Stack className="doctor_profile" direction={{ sm: 'column', lg: 'row' }} alignItems="center">
                              <Box>
                                <Box className="card-user-profile" style={user.profile_image_path ? { background: `url('https://mobileauth.imprimisrx.com/development/webservices/images/originals/${user.profile_image_path}')` } : {}}></Box>
                              </Box>
                              <Box className="card-user-desc">
                                <Typography className="heading-03-v1  heading-03-v1-size" variant="h3" component="h3">
                                  Welcome, <br /> {user.first_name + ' ' + user.last_name}
                                </Typography>
                                <Typography component="p" className="paragraph-03-v2">
                                  Here's what's happening in your account!
                                </Typography>
                                <Typography className="small">
                                  <span>Dr.</span>
                                  {user.first_name + ' ' + user.last_name} | <span>NPI:</span> {user.npi}{' '}
                                </Typography>
                              </Box>
                            </Stack>
                          )}
                        </Box>
                      </Box>
                      <Box padding={1} flex={{ xs: 1, md: 3 }}>
                        <Box className="card-sample-block">
                          <Box className="card_sample_in">
                            <Box className="most-used-block used-products-spotlight">
                              <Box className="most-used-heading">
                                <Box>
                                  <Typography variant="h4" component="h4" className="heading-04-v1" color="#263238CC">
                                    Products Spotlight
                                  </Typography>
                                </Box>
                              </Box>
                              {user.isApproved && (
                                <Box className="most-used-products">
                                  <Box className="most-used-pdt-list">
                                    <Stack className="product_spotlight" direction="row" alignItems="center" justifyContent="space-around">
                                      {productData &&
                                        productData?.map((data: Product, index: number) => {
                                          return (
                                            <Box className="most-used-pdt-list" key={index}>
                                              <Box className="product-short-img-block">
                                                <Box className="product-short-img" style={{ background: 'url(' + data.icon + ')' }}></Box>
                                              </Box>
                                              <Typography variant="h5" component="h5" className="same-typography-final">
                                                {data.package_name}
                                              </Typography>
                                            </Box>
                                          );
                                        })}
                                    </Stack>
                                  </Box>
                                  <Box className="view-all-link">
                                    <NavLink to="/home/product" style={{ textDecoration: 'none' }}>
                                      <div>Click here to view Full catalog</div>
                                    </NavLink>
                                  </Box>
                                </Box>
                              )}
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                    </Stack>
                    <Box padding={1} className="dashboardFlex">
                      <Box className="card-sample-block primary-border">
                        <Box>
                          <Typography variant="h4" component="h4" className="heading-04-v1" textAlign="center" color="#263238CC">
                            Start a Prescription
                          </Typography>
                          <Typography className="paragraph-01" textAlign="center" component="p" color="#263238CC">
                            Prescribe now by:{' '}
                          </Typography>
                        </Box>
                        {user.isApproved && (
                          <Stack className="start_prescription_btn" direction="row" justifyContent="center" gap={{ xs: 0, md: 3 }} paddingY={3} paddingX={8}>
                            <SecondaryButton onClick={handleMedication} label="Choose a Medication"></SecondaryButton>
                            <SecondaryButton onClick={handleNavigation} label="Choose existing Patient"></SecondaryButton>
                            <SecondaryButton onClick={handleClickOpen} label="Adding new Patient"></SecondaryButton>
                          </Stack>
                        )}
                      </Box>
                    </Box>
                  </Box>
                  <Stack direction={{ xs: 'column', md: 'column' }} style={{ minWidth: '200px' }}>
                    <Box padding={1} flex={{ md: 1, xl: 'unset' }}>
                      <Box className="card-sample-block statics-ratio-card">
                        <Box>
                          <Typography variant="h4" component="h4" className="heading-04-v1 --heading-v4-padding" textAlign="center" color="#263238CC">
                            Statistics
                          </Typography>
                        </Box>
                        <Stack textAlign="center" direction="row" alignItems="center" justifyContent="space-around" width="100%" className="stack-card-sample">
                          {user.isApproved && (
                            <Box className="card-sample-counting patients-counting">
                              <Typography variant="h1" component="h1">
                                {statisticsData.patients}
                              </Typography>
                              <Typography variant="h4" component="h4">
                                Patients
                              </Typography>
                            </Box>
                          )}

                          <Box className="card-counting-divider"></Box>
                          {user.isApproved && (
                            <Box className="card-sample-counting patients-counting">
                              <Typography variant="h1" component="h1">
                                {statisticsData.prescriptions}
                              </Typography>
                              <Typography variant="h4" component="h4">
                                Prescriptions
                              </Typography>
                            </Box>
                          )}
                        </Stack>
                      </Box>
                    </Box>
                    <Box padding={1} flex={{ xs: 1 }}>
                      <Carousel indicators={false} NextIcon={<img className="right_arrow" src={Right} alt="logo" height={15} width={15} />} PrevIcon={<img className="left_arrow" src={Left} alt="logo" height={15} width={15} />}>
                        <Box>
                          <Box className="card-sample-block card-sample-block-flex">
                            <Box className="exclusive-product-slide">
                              <Box className="exclusive-product-slide-desc">
                                <span className="exclusive-name-tag">1. 503B Exclusive</span>
                              </Box>
                              <Box className="exclusive-product-slide-desc">
                                <Typography variant="h3" component="h3">
                                  <span className="name-highlight">Cyclosporine 0.1%</span>
                                </Typography>
                              </Box>
                              <Stack direction="row" alignItems="flex-end" justifyContent="center">
                                <Box className="exclusive-product-slide-img">
                                  <Box className="product-slide-img"></Box>
                                </Box>
                                <Box className="exclusive-product-slide-desc">
                                  <Typography variant="h3" component="h3">
                                    Ophthalmic <br></br> Emulsion
                                    <span>(preservative-free)</span>
                                    5.5mL Bottles
                                  </Typography>
                                  <ul>
                                    <li>
                                      <span>»</span> No Prior Authorizations
                                    </li>
                                    <li>
                                      <span>»</span> No Coupons
                                    </li>
                                    <li>
                                      <span>»</span> No Pharmacy Call Backs
                                    </li>
                                  </ul>
                                  <Typography>
                                    <span>$440 per Box of 10</span> ($44 per bottle)
                                  </Typography>
                                </Box>
                              </Stack>
                              <Box className="order-used-btn">
                                <Button variant="contained" style={{ fontSize: '18px', backgroundColor: '#F3893D', borderRadius: '8px', boxShadow: 'none', height: '38px', width: '130px', textTransform: 'capitalize' }}>
                                  Order Now
                                </Button>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                        <Box>
                          <Box className="card-sample-block card-sample-block-flex">
                            <Box className="exclusive-product-slide">
                              <Box className="exclusive-product-slide-desc">
                                <span className="exclusive-name-tag">2. 503B Exclusive</span>
                              </Box>
                              <Box className="exclusive-product-slide-desc">
                                <Typography variant="h3" component="h3">
                                  <span className="name-highlight">Avenova 0.1%</span>
                                </Typography>
                              </Box>
                              <Stack direction="row" alignItems="flex-end" justifyContent="center">
                                <Box className="exclusive-product-slide-img">
                                  <Box className="product-slide-img1"></Box>
                                </Box>
                                <Box className="exclusive-product-slide-desc">
                                  <Typography variant="h3" component="h3">
                                    Ophthalmic <br></br> Emulsion
                                    <span>(preservative-free)</span>
                                    5.5mL Bottles
                                  </Typography>
                                  <ul>
                                    <li>
                                      <span>»</span> No Prior Authorizations
                                    </li>
                                    <li>
                                      <span>»</span> No Coupons
                                    </li>
                                    <li>
                                      <span>»</span> No Pharmacy Call Backs
                                    </li>
                                  </ul>
                                  <Typography>
                                    <span>$440 per Box of 10</span> ($44 per bottle)
                                  </Typography>
                                </Box>
                              </Stack>
                              <Box className="order-used-btn">
                                <Button variant="contained" style={{ fontSize: '18px', backgroundColor: '#F3893D', borderRadius: '8px', boxShadow: 'none', height: '38px', width: '130px', textTransform: 'capitalize' }}>
                                  Order Now
                                </Button>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                        <Box>
                          <Box className="card-sample-block card-sample-block-flex">
                            <Box className="exclusive-product-slide">
                              <Box className="exclusive-product-slide-desc">
                                <span className="exclusive-name-tag">3. 503B Exclusive</span>
                              </Box>
                              <Box className="exclusive-product-slide-desc">
                                <Typography variant="h3" component="h3">
                                  <span className="name-highlight"> Nicotinamide 0.1%</span>
                                </Typography>
                              </Box>
                              <Stack direction="row" alignItems="flex-end" justifyContent="center">
                                <Box className="exclusive-product-slide-img">
                                  <Box className="product-slide-img2"></Box>
                                </Box>
                                <Box className="exclusive-product-slide-desc">
                                  <Typography variant="h3" component="h3">
                                    Ophthalmic <br></br> Emulsion
                                    <span>(preservative-free)</span>
                                    5.5mL Bottles
                                  </Typography>
                                  <ul>
                                    <li>
                                      <span>»</span> No Prior Authorizations
                                    </li>
                                    <li>
                                      <span>»</span> No Coupons
                                    </li>
                                    <li>
                                      <span>»</span> No Pharmacy Call Backs
                                    </li>
                                  </ul>
                                  <Typography>
                                    <span>$440 per Box of 10</span> ($44 per bottle)
                                  </Typography>
                                </Box>
                              </Stack>
                              <Box className="order-used-btn">
                                <Button variant="contained" style={{ fontSize: '18px', backgroundColor: '#F3893D', borderRadius: '8px', boxShadow: 'none', height: '38px', width: '130px', textTransform: 'capitalize' }}>
                                  Order Now
                                </Button>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </Carousel>
                    </Box>
                  </Stack>
                </Stack>

                <Box padding={1}>
                  <Box className="card-sample-block">
                    <Box style={{ display: 'flex', paddingTop: '10px', alignItems: 'flex-end' }}>
                      <Box style={{ flex: 1 }}>
                        <Typography variant="h4" textAlign="center" component="h4" className="heading-04-v1" color="#263238CC">
                          Most Recent Orders
                        </Typography>
                      </Box>
                      <Box>
                        <Stack onClick={handleViewPage} style={{ cursor: 'pointer' }}>
                          <Box className="view_all_info">View All</Box>
                        </Stack>
                      </Box>
                    </Box>
                    <Box className="recent-order-table-layout">{user.isApproved && orderData && <OrderTable data={orderData} />}</Box>
                  </Box>
                </Box>
              </Stack>
            </Container>
          </Box>
          <Box className="chat-floating-icon">
            <img src={Chat} alt="logo" height={50} width={50} />
          </Box>
        </Stack>
      </>
    ): <></>
  );
}

import { Box, Container, FormControl, FormControlLabel, Grid, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { useEffect, useState } from "react";

import { AddNewPatient } from "../../../components/popup/AddNewPatient";
import { AddPrescriptionMedication } from "../../../components/popup/AddPrescriptionMedication";
import Chat from "../../../assets/icons/chat.svg";
import Deleat from "../../../assets/icons/deleat.svg";
import Deleatnew from "../../../assets/icons/deleat-new.svg";
import Dialog from "@mui/material/Dialog";
import Down from "../../../assets/icons/down_arrow.svg";
import DownLines from "../../../assets/icons/Line 66.svg";
import Edit_icon from "../../../assets/icons/edit.svg";
import Editnew from "../../../assets/icons/edit-new.svg";
import Ellipse from "../../../assets/icons/Ellipse.svg";
import Header from "../../../components/header/header";
import { InputDate } from "../../../core/forms/inputDate";
import NewPrescriptionPrimaryBtn from "../../../core/buttons/NewPrescriptionPrimaryBtn";
import PlusIcon from "../../../assets/icons/plus_icon.svg";
import Prescribe from "../../../assets/icons/prescribe_icon.svg";
import { PrescriptionInterface } from "../../../interfaces/prescription";
import { PrescriptionSearchBar } from "../../../components/PrescriptionSearchBar";
import { PrescriptionSearchTable } from "../../../components/tables/PrescriptionSearchTable";
import Uparrow from "../../../assets/icons/up-arrow.svg";
import User from "../../../assets/icons/user.svg";
import { isLoggedIn } from "../../../services/auth/auth.service";
import { styled } from "@mui/material/styles";
import { useAppSelector } from "../../../store";
import { useNavigate } from "react-router-dom";
import userIcon from "../../../assets/icons/userIcon.svg";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function PrescriptionOrderPage() {
  const [open, setOpen] = useState(false);
  const [prescription, setPrescription] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const router = useNavigate();
  const medicationData = useAppSelector(state => state.dashboardReducer.mediaction);

  const [addPrescriptionHistory, setAddPrescriptionHistory] = useState(false);
  const [isPrescriptionHistory, setIsPrescriptionHistory] = useState(false);

  const handleAddPrescriptionHistory = () => {
    setAddPrescriptionHistory(true);
    setIsPrescriptionHistory(false);
  }

  const handleAddPrescriptionHistoryNew = () => {
    setAddPrescriptionHistory(false);

    setIsPrescriptionHistory(true);
   
  }

  const [billData, setBillData] = useState({
    patient: false,
    prescriber: false,
  });
  const [addPatient, setAddPatient] = useState(false);
  const [oldBlock] = useState(true);
  const [newBlock, setNewBlock] = useState(false);
  const [shipData, setShipData] = useState({
    patient: false,
    prescriber: false,
  });
  const handleAddPatientBlock = () => {
    setAddPatient(true);
  };
  const handleBlockData = () => {
    setNewBlock(true);
  };
  const handlePatientBlock = () => {
    setNewBlock(true);
  };

  const handleShipPrescriberChange = () => {
    setShipData((oldData) => ({
      ...oldData,
      prescriber: true,
      patient: false,
    }));
  };

  const handleShipPatientChange = () => {
    setShipData((oldData) => ({
      ...oldData,
      patient: true,
      prescriber: false,
    }));
  };
  const handleBillPatientChange = () => {
    setBillData((oldData) => ({
      ...oldData,
      patient: true,
      prescriber: false,
    }));
  };

  const handleBillPrescriberChange = () => {
    setBillData((oldData) => ({
      ...oldData,

      prescriber: true,
      patient: false,
    }));
  };

  const [medication, setMedication] = useState(false);
  const [createPatient, setCreatePatient] = useState(false);
  const handleClose = () => {
    setMedication(false);
    setCreatePatient(false);
    setAddPrescriptionHistory(false);
    setOpen(false);
    setPrescription(false);
  };
  const prescriptionApiData: PrescriptionInterface[] = [];
  const handlePrescribeDetail = () => {
    router("/home/prescription-order");
  };
 
  const handleCreatePatient = () => {
    setCreatePatient(true);
  };
  useEffect(() => {
    console.log(medicationData);
  }, [medicationData]);

  console.log(medicationData);
  useEffect(() => {
    if (!isLoggedIn()) {
      router("/");
    } else {
      setIsLoaded(true);
    }
  }, []);

  return (
  <>
  {
  isLoaded && (
    <>
      <Stack component="main" className="default-layout">
        <Header />
        <Box>
          <Box component="main" className="patient-page">
            <Box className="prescription-ordering">
              <Container maxWidth="xl">
                <Box className="main-box">
                  <Box className="prescription_heading">Prescription Ordering</Box>

                  <Box className="orderline_box">
                    <Box className="order order_fill"></Box>
                    <Box className="order"></Box>
                    <Box className="order"></Box>
                    <Box className="order_line"></Box>
                  </Box>
                  <Box className="order_location">
                    <Box className="location start">
                      Build
                      <br></br>
                      Order
                    </Box>
                    <Box className="location">
                      Review
                      <br></br>
                      Order
                    </Box>
                    <Box className="location end">
                      Send to
                      <br></br>
                      Pharmacy
                    </Box>
                  </Box>
                  <Box className="ordersteps_main">
                    <Box className="steps_way">

                      <Box className="ordersteps_main">
                        <Box className="steps_way">
                          <Box className="steps_line"></Box>
                          <Box className="steps">
                            <Box className="step_start">Step 1</Box>
                            <Box className="place">Bill To</Box>
                            <Box className="back_processing">
                              <Box className="back_processingin"></Box>
                            </Box>
                          </Box>
                          <Container maxWidth="sm">
                            <Box className="nameof_customer">
                              <FormControl fullWidth>
                                <RadioGroup defaultValue="female" aria-labelledby="demo-customized-radios" name="customized-radios">
                                  <Grid container>
                                    <Stack className="radio_btn_stack" direction="row" justifyContent="space-between">
                                      <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                        <FormControlLabel value="female" control={<Radio onChange={handleBillPatientChange} />} label="patient" />
                                        {billData.patient && (
                                          <Typography className="contact_by_phone">
                                            <span>*</span> If patient card is not on file, ImprimisRx will contact patient by phone for payment.
                                          </Typography>
                                        )}
                                      </Grid>
                                      <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                        <FormControlLabel value="male" control={<Radio onChange={handleBillPrescriberChange} />} label="Prescriber" />
                                        {billData.prescriber && (
                                          <>
                                            <ul>
                                              <li className="edit">
                                                <span className="edit_icon">
                                                  <img src={Edit_icon} alt="logo" height={10} width={10} />
                                                </span>
                                                <Typography component="p">Edit</Typography>
                                              </li>
                                              <li>
                                                <span>Card ending in</span>
                                                XXXX-0458
                                              </li>
                                              <li className="dash">|</li>
                                              <li>
                                                <span>Billing ZIP</span> 37208
                                              </li>
                                            </ul>
                                          </>
                                        )}
                                      </Grid>
                                    </Stack>
                                  </Grid>
                                </RadioGroup>
                              </FormControl>
                            </Box>
                          </Container>
                          <Box className="steps">
                            <Box className="step_start">Step 2</Box>
                            <Box className="place">Ship To</Box>
                            <Box className="back_processing">
                              <Box className="back_processingin"></Box>
                            </Box>
                          </Box>
                          <Container maxWidth="sm">
                            <Box className="nameof_customer">
                              <FormControl fullWidth>
                                <RadioGroup defaultValue="female" aria-labelledby="demo-customized-radios" name="customized-radios">
                                  <Grid container>
                                    <Stack className="radio_btn_stack" direction="row" justifyContent="space-between">
                                      <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                        <FormControlLabel value="female" control={<Radio onChange={handleShipPatientChange} />} label="patient" />
                                        {shipData.patient && (
                                          <Typography className="contact_by_phone">
                                            <span>*</span> If patient card is not on file, ImprimisRx will contact patient by phone for payment.
                                          </Typography>
                                        )}
                                      </Grid>
                                      <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                        <FormControlLabel value="male" control={<Radio onChange={handleShipPrescriberChange} />} label="Prescriber" />
                                        {shipData.prescriber && (
                                          <>
                                            <ul>
                                              <li className="edit">
                                                <span className="edit_icon">
                                                  <img src={Edit_icon} alt="logo" height={10} width={10} />
                                                </span>
                                                <Typography component="p">Edit</Typography>
                                              </li>
                                              <li>
                                                <span>Card ending in</span>
                                                XXXX-0458
                                              </li>
                                              <li className="dash">|</li>
                                              <li>
                                                <span>Billing ZIP</span> 37208
                                              </li>
                                            </ul>
                                          </>
                                        )}
                                      </Grid>
                                    </Stack>
                                  </Grid>
                                </RadioGroup>
                              </FormControl>
                            </Box>
                          </Container>
                          <Box className="steps">
                            <Box className="step_start">Step 3</Box>
                            <Box className="place">Need by date</Box>
                            <Box className="back_processing">
                              <Box className="back_processingin"></Box>
                            </Box>
                          </Box>
                        </Box>
                        <Container maxWidth="sm">
                          <Box className="nameof_customer">
                            <FieldGroup
                              render={({ get, invalid }) => (
                                <form>
                                  <Stack className="date_pikker" direction="row" justifyContent="center" alignItems="center">
                                    <Typography component="h3">Select Date Here:</Typography>
                                    <Box className="date_input">
                                      <FieldControl name="date" render={InputDate} meta={{ label: "Select Date", placeholder: "Please Enter Patient Condition" }} />
                                    </Box>
                                  </Stack>
                                </form>
                              )}
                            />
                          </Box>
                        </Container>
                        <Stack className="down" alignItems="center" justifyContent="center">
                          <img src={DownLines} alt="logo" height={100} width={200} />
                        </Stack>
                        {oldBlock && (
                          <>
                            <Box className="add_patient_main">
                              <Container maxWidth="lg">
                                <Box className="steps_way">
                                  <Box className="steps_line"></Box>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 4</Box>

                                    {!addPatient && (
                                      <Box className="place" onClick={handleAddPatientBlock}>
                                        <span className="plus_icon">
                                          <img src={PlusIcon} alt="logo" height={14} width={14} />
                                        </span>
                                        Add Patient
                                      </Box>
                                    )}
                                    {addPatient && (
                                      <Box className="place_searchbar" onClick={handlePatientBlock}>
                                        <span className="plus_icon">
                                          <img src={userIcon} alt="logo" height={30} width={30} />
                                        </span>

                                        <Box className="recent-order-table-layouts">
                                          <PrescriptionSearchBar />
                                          <PrescriptionSearchTable data={prescriptionApiData} />

                                        </Box>
                                        <Box>
                                          <Stack className="start_prescription_btn" direction="row" justifyContent="center" gap={{ xs: 0, md: 3 }} paddingY={3} paddingX={8}>
                                            <NewPrescriptionPrimaryBtn onClick={handleCreatePatient} label="Create New Patient"></NewPrescriptionPrimaryBtn>
                                          </Stack>
                                        </Box>

                                      </Box>
                                    )}
                                    {createPatient && (
                                      <BootstrapDialog onClose={handleClose} open={createPatient} PaperProps={{ style: { minHeight: "84%", maxHeight: "89%", minWidth: "40%", maxWidth: 780 } }}>
                                        <AddNewPatient handleClose={handleClose} />
                                      </BootstrapDialog>
                                    )}

                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                  <Stack direction="row" justifyContent="center" alignItems="center">
                                    <Container maxWidth="sm">
                                      <Box className="detof_customer">
                                        <Box className="customer-number">1</Box>
                                        <ul>
                                          <li>
                                            <img src={User} alt="logo" height={20} width={20} />
                                          </li>
                                          <li>Theresa goodman</li>
                                          <li>|</li>
                                          <li>DOB: 05/04/1990</li>
                                          <li>
                                            <img src={Deleatnew} alt="logo" height={20} width={20} />
                                          </li>
                                        </ul>
                                        <Box className="down-arrow">
                                          <img src={Uparrow} alt="logo" height={20} width={20} />
                                        </Box>
                                      </Box>
                                    </Container>
                                  </Stack>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 5</Box>
                                    <Box className="place" onClick={handleAddPrescriptionHistoryNew}>
                                      <span className="plus_icon">
                                        <img src={PlusIcon} alt="logo" height={14} width={14} />
                                      </span>
                                      Add Prescription
                                    </Box>
                                    {addPrescriptionHistory && (
                                      <BootstrapDialog onClose={handleClose} open={addPrescriptionHistory} PaperProps={{ style: { minHeight: "50%", maxHeight: "65%", minWidth: "40%", maxWidth: 680 } }}>
                                        <AddPrescriptionMedication handleClose={handleClose} handleAddPrescriptionHistory={handleAddPrescriptionHistory}/>
                                      </BootstrapDialog>
                                    )}

                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                </Box>
                              </Container>

                              {isPrescriptionHistory && (
                              <Stack direction="row" justifyContent="center" alignItems="center">
                                <Container maxWidth="sm">
                                  <Box className="detof_prod">
                                    <Box className="product">
                                      <p>Product</p>
                                      <p>Price</p>
                                    </Box>
                                    <Box className="sale">
                                      <Box className="product-name">
                                        <span>
                                          <img src={Ellipse} alt="logo" height={50} width={30} />
                                        </span>
                                        Tim-Brim-Dor-Bim PF
                                      </Box>
                                      <Box className="product-price">
                                        <ul>
                                          <li>$59.95</li>
                                          <li>
                                            <img src={Editnew} alt="logo" height={20} width={20} />
                                          </li>
                                          <li>
                                            <img src={Deleatnew} alt="logo" height={20} width={20} />
                                          </li>
                                          <li>
                                            <img src={Down} alt="logo" height={20} width={20} />
                                          </li>
                                        </ul>
                                      </Box>
                                    </Box>
                                  </Box>
                                  {/* <Box className="detof_prod">
                                    <Box>
                                      <Box className="product">
                                        <p>Product</p>
                                        <p>Price</p>
                                      </Box>
                                      <Box className="sale">
                                        <Box className="product-name">
                                          <span>
                                            <Image src={Ellipse} alt="logo" height={50} width={30} />
                                          </span>
                                          Tim-Brim-Dor-Bim PF
                                        </Box>
                                        <Box className="product-price">
                                          <ul>
                                            <li>$59.95</li>
                                            <li>
                                              <Image src={Editnew} alt="logo" height={20} width={20} />
                                            </li>
                                            <li>
                                              <Image src={Deleatnew} alt="logo" height={20} width={20} />
                                            </li>
                                            <li>
                                              <Image src={Uparrow} alt="logo" height={20} width={20} />
                                            </li>
                                          </ul>
                                        </Box>
                                      </Box>
                                    </Box>
                                    <Box className="devider">
                                      <Box className="devider-line"></Box>
                                    </Box>
                                    <p>Medication details</p>
                                    <Container maxWidth="sm">
                                      <Box className="Medication">
                                        <Box className="quantity">
                                          <h4>Quantity</h4>
                                          <span>
                                            1 Vial (0.6mL)
                                            <Image src={Down} alt="logo" height={12} width={12} />
                                          </span>
                                          <h4>Day Supply</h4>
                                          <ul>
                                            <li>
                                              <span>
                                                <Image src={Minuse} alt="logo" height={10} width={10} />
                                              </span>
                                            </li>
                                            <li>0</li>
                                            <li>
                                              <span>
                                                <Image src={PlusIcon} alt="logo" height={10} width={10} />
                                              </span>
                                            </li>
                                          </ul>
                                        </Box>
                                        <Box className="dash"></Box>
                                        <Box className="quantity">
                                          <h4>Day Supply</h4>
                                          <span>Day Supply here</span>
                                          <h4>Day Supply</h4>
                                          <p>Storage Temperature here </p>
                                        </Box>
                                      </Box>
                                      <ul className="button-two">
                                        <li>
                                          <button className="btn cancle-btn">Cancel</button>
                                        </li>
                                        <li>
                                          <button className="btn save-btn">Save</button>
                                        </li>
                                      </ul>
                                    </Container>
                                  </Box> */}
                                </Container>
                              </Stack>
                             ) }
                              <Box className="cornar_box">Prescription Regimen 1</Box>
                              <Box className="deleat_icon">
                                <img src={Deleat} alt="logo" height={30} width={30} />
                              </Box>
                            </Box>
                          </>
                        )}

                        {newBlock && (
                          <>
                            <Box className="add_patient_main" mt={6}>
                              <Container maxWidth="md">
                                <Box className="steps_way">
                                  <Box className="steps_line"></Box>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 4</Box>
                                    <Box className="place">
                                      <span className="plus_icon">
                                        <img src={PlusIcon} alt="logo" height={14} width={14} />
                                      </span>
                                      Add Patient
                                    </Box>
                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                  <Container maxWidth="xs">
                                    <Box className="nameof_customer"></Box>
                                  </Container>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 5</Box>
                                    <Box className="place">
                                      <span className="plus_icon">
                                        <img src={PlusIcon} alt="logo" height={14} width={14} />
                                      </span>
                                      Add Prescription
                                    </Box>
                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                </Box>
                              </Container>
                              <Box className="cornar_box">Prescription Regimen 1</Box>
                              <Box className="deleat_icon">
                                <img src={Deleat} alt="logo" height={30} width={30} />
                              </Box>
                            </Box>
                          </>
                        )}

                        {newBlock && (
                          <>
                            <Box className="add_patient_main" mt={6} >
                              <Container maxWidth="md">
                                <Box className="steps_way">
                                  <Box className="steps_line"></Box>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 4</Box>
                                    <Box className="place">
                                      <span className="plus_icon">
                                        <img src={PlusIcon} alt="logo" height={14} width={14} />
                                      </span>
                                      Add Patient
                                    </Box>
                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                  <Container maxWidth="xs">
                                    <Box className="nameof_customer"></Box>
                                  </Container>
                                  <Box className="steps">
                                    <Box className="step_start step_start_border">Step 5</Box>
                                    <Box className="place">
                                      <span className="plus_icon">
                                        <img src={PlusIcon} alt="logo" height={14} width={14} />
                                      </span>
                                      Add Prescription
                                    </Box>
                                    <Box className="back_processing">
                                      <Box className="back_processingin"></Box>
                                    </Box>
                                  </Box>
                                </Box>
                              </Container>
                              <Box className="cornar_box">Prescription Regimen 1</Box>
                              <Box className="deleat_icon">
                                <img src={Deleat} alt="logo" height={30} width={30} />
                              </Box>
                            </Box>
                          </>


                        )
                        }

                        <Stack className="down down_last" direction="row" alignItems="center" justifyContent="center" onClick={handleBlockData}>
                          <span className="last_add_block">
                            <img src={PlusIcon} alt="logo" height={14} width={14} />
                          </span>
                          Add New Block
                        </Stack>
                      </Box>
                      <Box className="chat-floating-icon">
                        <img src={Chat} alt="logo" height={50} width={50} />
                      </Box>
                    </Box>
                  </Box>
                </Box>
              </Container>
            </Box>
          </Box>
        </Box>
      </Stack>
      <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
        <img src={Prescribe} alt="logo" height={100} width={150} />
      </Box>
    </>
  )

  }
  
  </>
  );
}
function setAddPrescriptionHistory(arg0: boolean) {
  throw new Error("Function not implemented.");
}


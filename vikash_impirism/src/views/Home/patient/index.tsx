import { Box, Container, Stack } from '@mui/material';
import { ChangeEvent, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store';

import { AddMedication } from '../../../components/popup/AddMedication';
import { AddNewPatient } from '../../../components/popup/AddNewPatient';
import { AxiosResponse } from 'axios';
import Chat from '../../../assets/icons/chat.svg';
import Dialog from '@mui/material/Dialog';
import Doctor from '../../../constants/grx-api';
import { GET_CURRENT_PATIENT } from '../../../constants/Endpoints';
import Header from '../../../components/header/header';
import { PatientInterface } from '../../../interfaces/patient';
import { PatientTable } from '../../../components/tables/PatientTable';
import Prescribe from '../../../assets/icons/prescribe_icon.svg';
import SearchBar from '../../../components/search-bar';
import { User } from '../../../models/User';
import { isLoggedIn } from '../../../services/auth/auth.service';
import { patientActions } from '../../../store/Actions';
import plusIcon from '../../../assets/icons/plus_icon.svg';
import { styled } from '@mui/material/styles';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2)
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1)
  }
}));

export default function PatientPage() {
  const router = useNavigate();
  const [open, setOpen] = useState(false);
  const [search, setSearch] = useState('');

  const setValue = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    if (!isLoggedIn()) {
      router('/');
    } else {
      setIsLoaded(true);
    }
  }, [router]);

  const [prescription, setPrescription] = useState(false);

  const handlePrescribeDetail = () => {
    router('/home/prescription-order');
  };

  const dispatch = useAppDispatch();
  const patientData: PatientInterface[] = useAppSelector((state) => state.patientReducer.patients);
  const user: User = useAppSelector((state) => state.profileReducer.user);

  const handlePrescription = () => {
    setPrescription(true);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setPrescription(false);
  };

  useEffect(() => {
    if (user) {
      getCurrentPatient();
    }
  }, [dispatch, user]);

  const getCurrentPatient = async () => {
    try {
      const res: AxiosResponse = await Doctor.get(GET_CURRENT_PATIENT, {
        params: { md_id: user?.md_id }
      });
      if (Array.isArray(res.data)) {
        dispatch(patientActions.setCurrentPatient(res.data));
      }
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };

  return (
    <>
      {isLoaded && (
        <>
          {open && (
            <BootstrapDialog onClose={handleClose} open={open} PaperProps={{ style: { minHeight: '84%', maxHeight: '89%', minWidth: '75%', maxWidth: 1298 } }}>
              <AddNewPatient handleClose={handleClose} />
            </BootstrapDialog>
          )}
          {prescription && (
            <BootstrapDialog onClose={handleClose} open={prescription} PaperProps={{ style: { minHeight: '84%', maxHeight: '89%', minWidth: ' 40%', maxWidth: 780 } }}>
              <AddMedication handleClose={handleClose} />
            </BootstrapDialog>
          )}

          <Stack component="main" className="default-layout">
            <Header />
            <Box>
              <Box component="main" className="patient-page">
                <Box className="main-content-wrapper-full">
                  <Container maxWidth="xl">
                    <Box className="main-box current_patient_main">
                      <Container maxWidth="xl">
                        <Box className="heading_top">Current Patients</Box>

                        {user.isApproved && (
                          <>
                            <Box className="add_btn_out">
                              <button className="add_btn" onClick={handleClickOpen}>
                                <span>
                                  <img src={plusIcon} alt="input_icon" width={12} />
                                </span>{' '}
                                Add Patient
                              </button>
                            </Box>

                            <SearchBar value={search} setValue={setValue}></SearchBar>

                            <Box className="recent-order-table-layout">
                              {patientData && (
                                <PatientTable
                                  data={patientData.filter((item: PatientInterface) => {
                                    return (search.length > 0 && (item.firstName + ' ' + item.lastName).toLowerCase().includes(search.toLowerCase())) || search.length === 0;
                                  })}
                                  onChange={handlePrescription}
                                />
                              )}
                            </Box>
                          </>
                        )}
                      </Container>

                      <Box className="chat-floating-icon">
                        <img src={Chat} alt="logo" height={65} width={65} />
                      </Box>
                      <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
                        <img src={Prescribe} alt="logo" height={100} width={180} />
                      </Box>
                    </Box>
                  </Container>
                </Box>
              </Box>
            </Box>
          </Stack>
        </>
      )}
    </>
  );
}

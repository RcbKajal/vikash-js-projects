import { AppBar, Box, Button, Container, Stack, Tab, Tabs, useTheme } from "@mui/material";
import { GET_CURRENT_PATIENT, GET_SINGLE_PATIENT } from "../../../constants/Endpoints";
import React, { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from "react";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import { useNavigate, useParams } from "react-router-dom";

import { AddMedication } from "../../../components/popup/AddMedication";
import { AddNewCard } from "../../../components/popup/AddNewCard";
import Add_white from "../../../assets/icons/add_white.svg";
import { AxiosResponse } from "axios";
import Back from "../../../assets/icons/back.svg";
import Chat from "../../../assets/icons/chat.svg";
import ContactProfile from "../../../components/ContactProfile";
import Dialog from "@mui/material/Dialog";
import Doctor from "../../../constants/grx-api";
import Edit from "../../../assets/icons/edit.svg";
import EditProfile from "../../../components/EditProfile";
import Header from "../../../components/header/header";
import Paper from "@mui/material/Paper";
import { PatientInterface } from "../../../interfaces/patient";
import Prescribe from "../../../assets/icons/prescribe_icon.svg";
import SearchBar from "../../../components/search-bar";
import Table from "@mui/material/Table";
import TableArrow from "../../../assets/icons/table_arrow.svg";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { isLoggedIn } from "../../../services/auth/auth.service";
import { styled } from "@mui/material/styles";
import { toast } from "react-toastify";

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: number;
  value: number;
}

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function createData(name: string, calories: number, fat: number, carbs: number, protein: number) {
  return { name, calories, fat, carbs, protein };
}

const rows = [createData("Frozen yoghurt", 159, 6.0, 24, 4.0), createData("Ice cream sandwich", 237, 9.0, 37, 4.3), createData("Eclair", 262, 16.0, 24, 6.0), createData("Cupcake", 305, 3.7, 67, 4.3), createData("Gingerbread", 356, 16.0, 49, 3.9)];

export default function Detail() {

  const [patient, setPatient]: [PatientInterface | null, Dispatch<SetStateAction<PatientInterface |null>> ] = useState<PatientInterface |null>(null);

  const params = useParams();
  const theme = useTheme();

  const [editProfile, setEditProfile] = useState(false);
  const router = useNavigate();

  const handleProfile = () => {
    // setEditProfile(true);
  };

  const handleBack = () => {
    // router.go();
  };

  const [data, setData] = useState({
    card: false,
    prescriber: false,
  });

  const [value, setValue] = React.useState(0);

  const [search, setSearch]: [string, Dispatch<string>] = React.useState<string>('');

  const setSearchValue = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  }

  const handlePrescription = () => {
    setData((oldData) => ({
      ...oldData,
      prescriber: true,
    }));
  };

  const handlePrescribeDetail = () => {
    router("/home/prescription-order");
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleClose = () => {
    setData((oldData) => ({
      ...oldData,
      card: false,
      prescriber: false,
    }));
  };


  const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
      <div role="tabpanel" hidden={value !== index} id={`full-width-tabpanel-${index}`} aria-labelledby={`full-width-tab-${index}`} {...other}>
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    );
  };

  const tabProps = (index: number) => {
    return {
      id: `full-width-tab-${index}`,
      "aria-controls": `full-width-tabpanel-${index}`,
    };
  };

  const getPatientDetail = async () => {
    try {
      const res: AxiosResponse = await Doctor.get(GET_SINGLE_PATIENT, {
        params: { patId: params.id }
      });
      if (res.data) {
        setPatient(res.data[0] as PatientInterface);
      }
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  }

  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (!isLoggedIn()) {
      router("/");
    } else {
      getPatientDetail();
      setIsLoaded(true);
    }
  }, []);


  return (
    <>
      {
        isLoaded && <>
          {data.card && (
            <BootstrapDialog
              onClose={handleClose}
              aria-labelledby="customized-dialog-title"
              open={data.card}
              PaperProps={{
                style: {
                  minHeight: "84%",
                  maxHeight: "89%",
                  minWidth: " 40%",
                  maxWidth: 686,
                },
              }}
            >
              <AddNewCard handleClose={handleClose} />
            </BootstrapDialog>
          )}
          {data.prescriber && (
            <BootstrapDialog
              onClose={handleClose}
              aria-labelledby="customized-dialog-title"
              open={data.prescriber}
              PaperProps={{
                style: {
                  minHeight: "84%",
                  maxHeight: "89%",
                  minWidth: " 40%",
                  maxWidth: 686,
                },
              }}
            >
              <AddMedication handleClose={handleClose} />
            </BootstrapDialog>
          )}

          <Stack component="main" className="default-layout">
            <Header />
            <Box component="main" className="setting-page" position="relative">
              <Box className="main-content-wrapper-full">
                <Container maxWidth="xl">
                  <Box className="main-content-wrap">
                    <Container maxWidth="xl">
                      <Box className="main-box">
                        <Box sx={{ bgcolor: "background.paper" }}>
                          <Box className="head_tabs_btn">
                            <AppBar position="static" className="tabs-bar">
                              <Tabs value={value} onChange={handleChange} style={{ background: "#ffffff" }} indicatorColor="primary" textColor="inherit" variant="fullWidth">
                                <Tab className="tab-btn" label="Contact & Profile" {...tabProps(0)} />
                                <Tab className="tab-btn" label="Prescription History" {...tabProps(1)} />
                              </Tabs>
                            </AppBar>
                          </Box>
                          <TabPanel value={value} index={0} dir={theme.direction}>
                            <Box>
                              <Box className="main-box">
                                <Box className="search"></Box>
                                {!editProfile && patient && <ContactProfile profile={patient} />}
                                {editProfile && <EditProfile />}
                                <Box className="back_patient">
                                  <Box className="patient_list" mr={3}>
                                    <span onClick={handleBack}>
                                      <img src={Back} alt="input_icon" width={30} height={30} />
                                    </span>
                                    <p>Back to Patients List</p>
                                  </Box>
                                  <Box className="patient_head">
                                    <ul>
                                      <li>
                                        <Button className="contact_btn" variant="contained" onClick={handlePrescription} style={{ fontSize: "17px", height: "49", width: "212", fontWeight: "600", backgroundColor: "#00ACBA", border: "1px solid #00ACBA", borderRadius: "8px", boxShadow: "none", textTransform: "capitalize" }}>
                                          {" "}
                                          <Box mr={1}>
                                            <img src={Add_white} alt="input_icon" width={15} />
                                          </Box>{" "}
                                          Add Prescription
                                        </Button>
                                      </li>
                                      <li>
                                        <Button className="contact_btn" variant="outlined" style={{ color: "#00ACBA", fontSize: "17px", height: "49", width: "212", fontWeight: "600", backgroundColor: "#fff", border: "2px solid #00ACBA", borderRadius: "8px", boxShadow: "none", textTransform: "capitalize" }} onClick={handleProfile}>
                                          {" "}
                                          <Box mr={1}>
                                            <img src={Edit} alt="input_icon" width={15} />
                                          </Box>{" "}
                                          Edit Info
                                        </Button>
                                      </li>
                                    </ul>
                                  </Box>
                                </Box>
                              </Box>
                            </Box>
                          </TabPanel>
                          <TabPanel value={value} index={1} dir={theme.direction}>
                            <Box className="payment">
                              <Container maxWidth="xl">
                                <Stack className="Contact_img" display="flex" alignItems="center" justifyContent="center">
                                  <Box className="tw_icon_out">
                                    <span className="tw_icon">TW</span>
                                    Theresa Woodman
                                  </Box>
                                </Stack>
                                <Box className="recent-order-table-layout recents-order-table-patient">
                                  <Box className="search_box">
                                    <SearchBar value={search} setValue={setSearchValue} ></SearchBar>
                                  </Box>

                                  <TableContainer sx={{ maxHeight: 200 }} component={Paper} className="table_customized">
                                    <Table sx={{ minWidth: 700 }} stickyHeader>
                                      <TableHead className="table_head">
                                        <TableRow>
                                          <StyledTableCell>
                                            Rx Number{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Date Written{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Prescriber{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Medication{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Status{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Tracking #{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                          <StyledTableCell>
                                            Order ID{" "}
                                            <span>
                                              <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
                                            </span>
                                          </StyledTableCell>
                                        </TableRow>
                                      </TableHead>
                                      <TableBody className="table_body">
                                        {rows.map((row) => (
                                          <StyledTableRow key={row.name}>
                                            <StyledTableCell component="td" className="table_first">
                                              <Stack>Rx-65964845</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td">
                                              <Stack>11/10/2021</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td">
                                              <Stack>Jennifer Acosta</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td">
                                              <Stack>Dex-Moxi-Ketor®</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td">
                                              <Stack>Pharmacy</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td" className="table_first">
                                              <Stack>Track Now</Stack>
                                            </StyledTableCell>
                                            <StyledTableCell component="td" className="table_first">
                                              <Stack>ORD-87209514</Stack>
                                            </StyledTableCell>
                                          </StyledTableRow>
                                        ))}
                                      </TableBody>
                                    </Table>
                                  </TableContainer>
                                </Box>
                                <Box className="back_patient">
                                  <Box className="patient_list" mr={3}>
                                    <span onClick={handleBack}>
                                      <img src={Back} alt="input_icon" width={30} height={30} />
                                    </span>
                                    <p>Back to Patients List</p>
                                  </Box>
                                </Box>
                              </Container>
                            </Box>
                          </TabPanel>
                        </Box>
                      </Box>
                    </Container>
                  </Box>
                </Container>
              </Box>
              <Box className="chat-floating-icon">
                <img src={Chat} alt="logo" height={50} width={50} />
              </Box>
              <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
                <img src={Prescribe} alt="logo" height={100} width={150} />
              </Box>
            </Box>
          </Stack>
        </>
      }

    </>
  );
}

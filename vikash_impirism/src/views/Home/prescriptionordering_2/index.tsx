import { Box, Container, FormControl, FormControlLabel, Grid, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { useEffect, useState } from "react";

import { AddMedication } from "../../../components/popup/AddMedication";
import { AddNewPatient } from "../../../components/popup/AddNewPatient";
import Chat from "../../src/assets/icons/chat.svg";
import CheckWhite from "../../src/assets/icons/chacked_white.svg";
import Deleat from "../../src/assets/icons/deleat.svg";
import Dialog from "@mui/material/Dialog";
import DownLines from "../../src/assets/icons/Line 66.svg";
import Edit_icon from "../../src/assets/icons/edit.svg";
import Header from "../../../components/header/header";
import { InputSelect } from "../../../core/forms/inputs/InputSelect";
import { InputText } from "../../../core/forms/inputs/InputText";
import PlusIcon from "../../src/assets/icons/plus_icon.svg";
import Prescribe from "../../src/assets/icons/prescribe_icon.svg";
import { notificationOptions } from "../../../services/components/selectOptions.service";
import { styled } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function Index() {
  const router = useNavigate();
  const [isLoaded, setIsLoaded] = useState(false);
  const [open, setOpen] = useState(false);
  const [prescription, setPrescription] = useState(false);

  useEffect(() => {
    setIsLoaded(true);
  }, []);
  const handlePrescribeDetail = () => {
    router("/home/prescription-order");
  };

  const handleClose = () => {
    setOpen(false);
    setPrescription(false);
  };

  return (
    isLoaded && (
      <>
        {open && (
          <BootstrapDialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={open}
            PaperProps={{
              style: {
                minHeight: "84%",
                maxHeight: "89%",
                minWidth: "75%",
                maxWidth: 1298,
              },
            }}
          >
            <AddNewPatient handleClose={handleClose} />
          </BootstrapDialog>
        )}
        {prescription && (
          <BootstrapDialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={prescription}
            PaperProps={{
              style: {
                minHeight: "84%",
                maxHeight: "89%",
                minWidth: " 40%",
                maxWidth: 780,
              },
            }}
          >
            <AddMedication handleClose={handleClose} />
          </BootstrapDialog>
        )}
        <Stack component="main" className="default-layout">
          <Header />
          <Box>
            <Box component="main" className="patient-page">
              <Box className="prescription-ordering">
                <Container maxWidth="xl">
                  <Box className="main-box">
                    <Box className="prescription_heading">Prescription Ordering</Box>
                    {/*------ ORDER LINE SECTION HERE ------*/}
                    <Box className="orderline_box">
                      <Box className="order order_fill"></Box>
                      <Box className="order"></Box>
                      <Box className="order"></Box>
                      <Box className="order_line"></Box>
                    </Box>
                    {/*------ ORDER LOCATION NAME SECTION HERE ------*/}
                    <Box className="order_location">
                      <Box className="location start">
                        Build
                        <br></br>
                        Order
                      </Box>
                      <Box className="location">
                        Review
                        <br></br>
                        Order
                      </Box>
                      <Box className="location end">
                        Send to
                        <br></br>
                        Pharmacy
                      </Box>
                    </Box>
                    {/*------ ORDER STEPS SECTION HERE ------*/}
                    <Box className="ordersteps_main">
                      {/*------ ORDER STEP ONE SECTION HERE ------*/}
                      <Box className="steps_way">
                        <Box className="steps_line"></Box>
                        <Box className="steps">
                          <Box className="step_start">Step 1</Box>
                          <Box className="place">Select a Prescriber</Box>
                          <Box className="back_processing">
                            <img src={CheckWhite} alt="logo" height={22} width={25} />
                          </Box>
                        </Box>
                        <Container maxWidth="xs">
                          <Box className="nameof_customer">
                            <FieldGroup
                              render={({ get, invalid }) => (
                                <form>
                                  <FieldControl name="gender" render={InputSelect} meta={{ name: "gender", value: "gender", options: notificationOptions, label: "Jennifer Acosta", placeholder: "Please Enter Gender" }} />
                                </form>
                              )}
                            />
                          </Box>
                        </Container>
                        <Box className="steps">
                          <Box className="step_start">Step 2</Box>
                          <Box className="place">Bill To</Box>
                          <Box className="back_processing">
                            <Box className="back_processingin"></Box>
                          </Box>
                        </Box>
                        <Container maxWidth="sm">
                          <Box className="nameof_customer">
                            <FormControl fullWidth>
                              <RadioGroup defaultValue="female" aria-labelledby="demo-customized-radios" name="customized-radios">
                                <Grid container>
                                  <Stack className="radio_btn_stack" direction="row" justifyContent="space-between">
                                    <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                      <FormControlLabel value="female" control={<Radio />} label="patient" />
                                    </Grid>
                                    <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                      <FormControlLabel value="male" control={<Radio />} label="Prescriber" />
                                      <ul>
                                        <li className="edit">
                                          <span className="edit_icon">
                                            <img src={Edit_icon} alt="logo" height={10} width={10} />
                                          </span>
                                          <Typography component="p">Edit</Typography>
                                        </li>
                                        <li>
                                          <span>Card ending in</span>
                                          XXXX-0458
                                        </li>
                                        <li className="dash">|</li>
                                        <li>
                                          <span>Billing ZIP</span> 37208
                                        </li>
                                      </ul>
                                    </Grid>
                                  </Stack>
                                </Grid>
                              </RadioGroup>
                            </FormControl>
                          </Box>
                        </Container>
                        <Box className="steps">
                          <Box className="step_start">Step 3</Box>
                          <Box className="place">Ship To</Box>
                          <Box className="back_processing">
                            <Box className="back_processingin"></Box>
                          </Box>
                        </Box>
                        <Container maxWidth="sm">
                          <Box className="nameof_customer">
                            <FormControl fullWidth>
                              <RadioGroup defaultValue="female" aria-labelledby="demo-customized-radios" name="customized-radios">
                                <Grid container>
                                  <Stack className="radio_btn_stack" direction="row" justifyContent="space-between">
                                    <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                      <FormControlLabel value="female" control={<Radio />} label="patient" />
                                    </Grid>
                                    <Grid className="radio_grid" sm={6} md={6} lg={6}>
                                      <FormControlLabel value="male" control={<Radio />} label="Prescriber" />
                                      <ul>
                                        <li className="edit">
                                          <span className="edit_icon">
                                            <img src={Edit_icon} alt="logo" height={10} width={10} />
                                          </span>
                                          <Typography component="p">Edit</Typography>
                                        </li>
                                        <li>105 Rockcastle Dr, Nashville, TN 37248</li>
                                      </ul>
                                    </Grid>
                                  </Stack>
                                </Grid>
                              </RadioGroup>
                            </FormControl>
                          </Box>
                        </Container>
                        <Box className="steps">
                          <Box className="step_start">Step 4</Box>
                          <Box className="place">Need by date</Box>
                          <Box className="back_processing">
                            <Box className="back_processingin"></Box>
                          </Box>
                        </Box>
                      </Box>
                      <Container maxWidth="sm">
                        <Box className="nameof_customer">
                          <FieldGroup
                            render={({ get, invalid }) => (
                              <form>
                                <Stack className="date_pikker" direction="row" justifyContent="center" alignItems="center">
                                  <Typography component="h3">Select Date Here:</Typography>
                                  <Box className="date_input">
                                    <FieldControl name="firstName" render={InputText} meta={{ label: "Patient Condition", placeholder: "Please Enter Patient Condition" }} />
                                  </Box>
                                </Stack>
                              </form>
                            )}
                          />
                        </Box>
                      </Container>
                      {/*------ DOWN ARROW SECTION HERE ------*/}
                      <Stack className="down" alignItems="center" justifyContent="center">
                        <img src={DownLines} alt="logo" height={100} width={200} />
                      </Stack>
                      {/*------ ADD PATIENT SECTION HERE ------*/}
                      <Box className="add_patient_main">
                        <Container maxWidth="md">
                          <Box className="steps_way">
                            <Box className="steps_line"></Box>
                            <Box className="steps">
                              <Box className="step_start step_start_border">Step 5</Box>
                              <Box className="place">
                                <span className="plus_icon">
                                  <img src={PlusIcon} alt="logo" height={14} width={14} />
                                </span>
                                Add Patient
                              </Box>
                              <Box className="back_processing">
                                <Box className="back_processingin"></Box>
                              </Box>
                            </Box>
                            <Container maxWidth="xs">
                              <Box className="nameof_customer"></Box>
                            </Container>
                            <Box className="steps">
                              <Box className="step_start step_start_border">Step 6</Box>
                              <Box className="place">
                                <span className="plus_icon">
                                  <img src={PlusIcon} alt="logo" height={14} width={14} />
                                </span>
                                Add Prescription
                              </Box>
                              <Box className="back_processing">
                                <Box className="back_processingin"></Box>
                              </Box>
                            </Box>
                          </Box>
                        </Container>
                        <Box className="cornar_box">Prescription Regimen 1</Box>
                        <Box className="deleat_icon">
                          <img src={Deleat} alt="logo" height={30} width={30} />
                        </Box>
                      </Box>
                      {/*------ DOWN ARROW SECTION HERE ------*/}
                      <Stack className="down down_last" direction="row" alignItems="center" justifyContent="center">
                        <span className="last_add_block">
                          <img src={PlusIcon} alt="logo" height={14} width={14} />
                        </span>
                        Add New Block
                      </Stack>
                    </Box>
                    <Box className="chat-floating-icon">
                      <img src={Chat} alt="logo" height={50} width={50} />
                    </Box>
                  </Box>
                </Container>
              </Box>
            </Box>
          </Box>
        </Stack>
        <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
          <img src={Prescribe} alt="logo" height={100} width={150} />
        </Box>
      </>
    )
  );
}

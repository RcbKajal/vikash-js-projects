import { ADD_FAV_PRODUCT, PRODUCT_CATALOG, PRODUCT_CATALOG_CAT_FILTER, PRODUCT_Dosage_FILTER, REMOVE_FAV_PRODUCT } from '../../../constants/Endpoints';
import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Container, FormControlLabel, Grid, Stack, Typography } from '@mui/material';
import React, { ChangeEvent, Dispatch, useCallback, useEffect, useState } from 'react';
import { RootState, useAppDispatch, useAppSelector } from '../../../store';

import { AxiosError } from '../../../interfaces/api';
import { AxiosResponse } from 'axios';
import { CategoryInterface } from '../../../interfaces/category';
import Chat from '../../../assets/icons/chat.svg';
import { DosageInterface } from '../../../interfaces/dosage';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Header from '../../../components/header/header';
import PhysicianDoctor from '../../../constants/api';
import Prescribe from '../../../assets/icons/prescribe_icon.svg';
import { Product } from '../../../models/Product';
import Product_ins from '../../../assets/icons/product_insert.svg';
import SearchBar from '../../../components/search-bar';
import { User } from '../../../models/User';
import favFilled from '../../../assets/icons/fav-filled.svg';
import favOutline from '../../../assets/icons/fav-outline.svg';
import { isLoggedIn } from '../../../services/auth/auth.service';
import { productActions } from '../../../store/Actions';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const ProductPage = () => {
  const dispatch = useAppDispatch();
  const router = useNavigate();
  const ProductCatalogData = useAppSelector((state: RootState) => state.productReducer.products);

  const handlePrescribeDetail = () => {
    router('/home/prescription-order');
  };

  const [search, setSearch] = React.useState('');
  const setValue = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e?.target?.value);
  };

  const [categories, setCategories]: [CategoryInterface[], Dispatch<CategoryInterface[]>] = useState<CategoryInterface[]>([]);
  const [categoryFilters, setCategoryFilters]: [string[], Dispatch<string[]>] = useState<string[]>([]);

  const [dosageFilters, setDosageFilters]: [DosageInterface[], Dispatch<DosageInterface[]>] = useState<DosageInterface[]>([]);
  const [selectedDosageFilters, setSelectedDosageFilters]: [DosageInterface[], Dispatch<DosageInterface[]>] = useState<DosageInterface[]>([]);
  const [favFilter, setFavFilter] = useState(false);

  const user: User = useAppSelector((state: RootState) => state.profileReducer.user);

  const addFav = async(id: string) => {
    try {
      const res: AxiosResponse =  await PhysicianDoctor.post(ADD_FAV_PRODUCT, {}, { params: { api_key: localStorage.getItem('api_key'), product_id: id, doctor_id: user.doctor_id } });
      if(res.data.status === "OK"){
        toast('Added to favorite');
        load();
      } 
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }    
  };

  const removeFav = async(id: string) => {
    try {
      const res: AxiosResponse =  await PhysicianDoctor.post(REMOVE_FAV_PRODUCT, {}, { params: { api_key: localStorage.getItem('api_key'), product_id: id, doctor_id: user.doctor_id } })
      if(res.data.status === "OK"){
        toast('Removed from favorite');
        load();
      }
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }

  };

  const handleFav = (product: Product) => {
    if (product.is_favourite) {
      removeFav(product.product_id);
    } else {
      addFav(product.product_id);
    }
  };

  const isAllChildChecked = (childs: string[]) => {
    return childs.length > 0 && childs.filter((el: string) => !categoryFilters.includes(el)).length === 0;
  };

  const categoryChecked = (e: ChangeEvent<HTMLInputElement>, cat: CategoryInterface) => {
    const newCats = [cat.package_id];
    for (let child of cat.Child) {
      newCats.push(child.package_id);
    }
    if (e.target.checked) {
      setCategoryFilters([...categoryFilters, ...newCats]);
    } else {
      newCats.push(cat.parent_id);
      setCategoryFilters(categoryFilters.filter((el: string) => !newCats.includes(el)));
    }
  };

  const dosageChecked = (e: ChangeEvent<HTMLInputElement>, cat: DosageInterface) => {
    if (e.target.checked) {
      setSelectedDosageFilters([...selectedDosageFilters, cat]);
    } else {
      setSelectedDosageFilters(selectedDosageFilters.filter((el: DosageInterface) => !(cat.df_id === el.df_id)));
    }
  };

  const load = useCallback( async () => {
    try {
      const params: {
        api_key: string | null;
        dosage_forms?: string;
      } = { api_key: localStorage.getItem('api_key') };
      if (selectedDosageFilters.length > 0) {
        params.dosage_forms = selectedDosageFilters.map((item: DosageInterface) => item.df_id).join();
      }
      const res: AxiosResponse = await PhysicianDoctor.post(PRODUCT_CATALOG, {}, { params }); 
          const productData = res.data.response;
          if (productData !== null) {
            dispatch(productActions.setProductCatalogData({ data: productData }));
          }
      
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  
  }, [dispatch, selectedDosageFilters]);

  const loadCategoryFilter = async () => {
    try {
      const res: AxiosResponse = await PhysicianDoctor.post(PRODUCT_CATALOG_CAT_FILTER, {}, { params: { api_key: localStorage.getItem('api_key') } });
      setCategories(res.data.response);
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };

  const loadDosageFilter = async () => {
    try {
      const res: AxiosResponse = await PhysicianDoctor.post(PRODUCT_Dosage_FILTER, {}, { params: { api_key: localStorage.getItem('api_key'), constant_id: 6 } });
      setDosageFilters(res.data.response);
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };

  useEffect(() => {
    load();
    loadCategoryFilter();
    loadDosageFilter();
  }, [load]);

  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    if (!isLoggedIn()) {
      router('/');
    } else {
      setIsLoaded(true);
    }
  }, [router]);

  return (
    <>
      {isLoaded && (
        <Stack component="main" className="default-layout">
          <Header />
          <Box component="main" className="product-page">
            <Box className="main-content-wrapper-full">
              <Container maxWidth="xl">
                <Box className="main-content-wrap main-box product_page">
                  <Box className="heading_top">Product Catalog</Box>
                  <Stack direction="row" alignItems="center" className="productLayout">
                    <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} className="product_content">
                      <Grid item xs={12} sm={12} md={12} lg={3} className="product_content_top">
                        <Box className="product_dropdown_side">
                          <Box className="accordian">
                            <Accordion className="accordian_btn">
                              <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                                <Typography>Product Category</Typography>
                              </AccordionSummary>
                              <AccordionDetails>
                                {categories &&
                                  categories.map((cat: CategoryInterface, index: number) => {
                                    return (
                                      <div key={index} className="checkbox_outer">
                                        <FormControlLabel
                                          sx={{ margin: 0 }}
                                          control={
                                            <Box marginRight={1}>
                                              <label className="check-input">
                                                <input type="checkbox" onChange={(e) => categoryChecked(e, cat)} checked={categoryFilters.includes(cat.package_id) || isAllChildChecked(cat.Child.map((c: CategoryInterface) => c.package_id))} />
                                                <span className="checkmark"></span>
                                              </label>
                                            </Box>
                                          }
                                          label={cat.name}
                                        />

                                        {cat.Child &&
                                          cat.Child.map((subCat: CategoryInterface, i: number) => {
                                            return (
                                              <div key={i} className="checkbox_outer child" style={{ paddingLeft: '30px' }}>
                                                <FormControlLabel
                                                  sx={{ margin: 0 }}
                                                  control={
                                                    <Box marginRight={1}>
                                                      <label className="check-input">
                                                        <input type="checkbox" onChange={(e) => categoryChecked(e, subCat)} checked={categoryFilters.includes(subCat.package_id)} />
                                                        <span className="checkmark"></span>
                                                      </label>
                                                    </Box>
                                                  }
                                                  label={subCat.name}
                                                />
                                              </div>
                                            );
                                          })}
                                      </div>
                                    );
                                  })}
                              </AccordionDetails>
                            </Accordion>
                            <Accordion className="accordian_btn">
                              <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2a-content" id="panel2a-header">
                                <Typography>Dosage Form</Typography>
                              </AccordionSummary>
                              <AccordionDetails>
                                {dosageFilters &&
                                  dosageFilters.map((cat: DosageInterface, index: number) => {
                                    return (
                                      <div key={index} className="checkbox_outer">
                                        <FormControlLabel
                                          sx={{ margin: 0 }}
                                          control={
                                            <Box marginRight={1}>
                                              <label className="check-input">
                                                <input type="checkbox" onChange={(e) => dosageChecked(e, cat)} />
                                                <span className="checkmark"></span>
                                              </label>
                                            </Box>
                                          }
                                          label={cat.df_name}
                                        />
                                      </div>
                                    );
                                  })}
                              </AccordionDetails>
                            </Accordion>
                          </Box>

                          <Box className="filter">
                            <ul style={{ cursor: 'pointer' }} onClick={() => setFavFilter(!favFilter)}>
                              <li>
                                <img src={favFilter ? favFilled : favOutline} height={25} width={25} alt="fav" />
                              </li>
                              <li>My Favorites</li>
                            </ul>
                          </Box>
                        </Box>
                      </Grid>
                      <Grid item xs={12} sm={12} md={12} lg={9} className="product_content_bottom">
                        <SearchBar value={search} setValue={setValue}></SearchBar>

                        <Box className="product_scroll">
                          <Stack className="product_stack" direction="row" flex-wrap="wrap" alignItems="center" justifyContent="center">
                            {ProductCatalogData &&
                              ProductCatalogData.filter((item: Product) => {
                                return (search.length > 0 && (item.medication_name + ' ' + item.ndc + item.package_name).toLowerCase().includes(search.toLowerCase())) || search.length === 0;
                              })
                                .filter((item: Product) => {
                                  if (!favFilter) {
                                    return true;
                                  }
                                  return item.is_favourite === true;
                                })
                                .filter((item: Product) => {
                                  if (categoryFilters.length === 0) {
                                    return true;
                                  }

                                  return categoryFilters.includes(item.package_id) || categoryFilters.includes(item.sub_pkg_id);
                                })
                                .map((data: Product, index: number) => {
                                  return (
                                    <Box className="product_out" key={index}>
                                      <Box className="product_outerInfo_content">
                                        <Box className="productInfos">
                                          <Box className="product-short-img-block">
                                            <Box className="product-short-img" style={{ background: 'url(' + data.icon + ')' }}></Box>
                                          </Box>
                                          <Typography className="Product_name" style={{ lineHeight: '1.2' }}>
                                            {data?.medication_name}
                                          </Typography>
                                          <Typography className="Product_qty">{data?.Variations[0]?.size}</Typography>
                                        </Box>
                                        <Box className="productInfosbtn">
                                          <Button
                                            variant="outlined"
                                            style={{
                                              color: '#00ACBA',
                                              fontSize: '18px',
                                              fontWeight: '600',
                                              backgroundColor: '#fff',
                                              border: '1px solid #00ACBA',
                                              borderRadius: '8px',
                                              boxShadow: 'none',
                                              height: '39px',
                                              width: '200px',
                                              marginInline: 'auto',
                                              textTransform: 'capitalize',
                                              marginTop: '6px'
                                            }}
                                            onClick={handlePrescribeDetail}
                                          >
                                            Prescribe
                                          </Button>
                                        </Box>
                                      </Box>

                                      <Box className="favorite_product">
                                        <ul>
                                          <li className="fav-icon-outer">
                                            <img src={data.is_favourite ? favFilled : favOutline} alt="fav" height={17} onClick={() => handleFav(data)} />
                                          </li>
                                          <li>
                                            <img src={Product_ins} alt="logo" height={17} />
                                            <Typography component="p">
                                              Product<br></br>
                                              Insert
                                            </Typography>
                                          </li>
                                        </ul>
                                      </Box>
                                    </Box>
                                  );
                                })}
                          </Stack>
                        </Box>
                      </Grid>
                    </Grid>
                  </Stack>
                </Box>
              </Container>
            </Box>
          </Box>
          <Box className="chat-floating-icon">
            <img src={Chat} alt="logo" height={65} width={65} />
          </Box>
          <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
            <img src={Prescribe} alt="logo" height={100} width={180} />
          </Box>
        </Stack>
      )}
    </>
  );
};
export default ProductPage;

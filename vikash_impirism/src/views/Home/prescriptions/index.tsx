import { Box, Container, Stack } from '@mui/material';
import React, { ChangeEvent, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../../store';

import { AddNewPatient } from '../../../components/popup/AddNewPatient';
import { AxiosResponse } from 'axios';
import Chat from '../../../assets/icons/chat.svg';
import Dialog from '@mui/material/Dialog';
import Doctor from '../../../constants/grx-api';
import { GET_PRESCRIPTION_HISTORY } from '../../../constants/Endpoints';
import Header from '../../../components/header/header';
import Prescribe from '../../../assets/icons/prescribe_icon.svg';
import { PrescriptionHistoryTable } from '../../../components/tables/PrescriptionHistoryTable';
import { PrescriptionInterface } from '../../../interfaces/prescription';
import SearchBar from '../../../components/search-bar';
import { User } from '../../../models/User';
import { prescriptionActions } from '../../../store/Actions';
import { styled } from '@mui/material/styles';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2)
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1)
  }
}));

export default function PrescriptionPage() {
  const router = useNavigate();
  const dispatch = useAppDispatch();
  const user: User = useAppSelector((state) => state.profileReducer.user);
  const prescriptionData = useAppSelector((state) => state.prescriptionReducer.prescription);

  const [open, setOpen] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const setValue = (e: ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const handlePrescribeDetail = () => {
    router('/home/prescription-order');
  };

  const handleClose = () => {
    setOpen(false);
  };

  // recent orders
  useEffect(() => {
    if (user) {
      getPrescriptionHistory();
    }
  }, [dispatch, user]);

  const getPrescriptionHistory = async () => {
    try {
      const res: AxiosResponse = await Doctor.get(GET_PRESCRIPTION_HISTORY, { params: { md_id: user?.md_id } });
      dispatch(prescriptionActions.setPrescriptionHistory(res.data));
    } catch (err: any) {
      if (err?.response?.data?.status === 'Error') {
        toast(err?.response.data.message);
        return;
      }
    }
  };
  return (
    <>
      {open && (
        <BootstrapDialog onClose={handleClose} open={open} PaperProps={{ style: { minHeight: '84%', maxHeight: '89%', minWidth: '75%', maxWidth: 1298 } }}>
          <AddNewPatient handleClose={handleClose} />
        </BootstrapDialog>
      )}
      <Stack component="main" className="default-layout">
        <Header />
        <Box>
          <Container maxWidth="xl">
            <Box component="main" className="patient-page">
              <Box className="main-content-wrapper-full">
                <Box className="main-box prescriptions_main">
                  <Container maxWidth="xl">
                    <Box className="heading_top">Prescription History</Box>

                    {user.isApproved && (
                      <>
                        <SearchBar value={search} setValue={setValue}></SearchBar>

                        <Box className="recent-order-table-layout">
                          {prescriptionData && (
                            <PrescriptionHistoryTable
                              data={prescriptionData.filter((item: PrescriptionInterface) => {
                                return (search.length > 0 && (item.Prescription_Number + ' ' + item.Patient_Name + ' ' + item.Product_Name).toLowerCase().includes(search.toLowerCase())) || search.length === 0;
                              })}
                            />
                          )}
                        </Box>
                      </>
                    )}
                  </Container>
                </Box>
              </Box>
            </Box>
          </Container>
          <Box className="chat-floating-icon">
            <img src={Chat} alt="logo" height={65} width={65} />
          </Box>
          <Box className="Prescribe-icon" onClick={handlePrescribeDetail}>
            <img src={Prescribe} alt="logo" height={100} width={180} />
          </Box>
        </Box>
      </Stack>
    </>
  );
}

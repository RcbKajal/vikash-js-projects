import '../theme/style.scss';
import 'react-toastify/dist/ReactToastify.css';

import { Outlet, Route, Routes } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '../store';
import { useEffect, useState } from 'react';

import DashboardPage from './Home/dashboard';
import Detail from './Home/patient/detail';
import DonePage from './Auth/forgot-password/reset-password/done';
import FavouritePage from './Home/favorites';
import ForgotPassword from './Auth/forgot-password';
import { LoginPage } from "./Auth/Login";
import OrderPage from './Home/prescriptions/order';
import PatientPage from './Home/patient';
import PrescriptionOrderPage from './Home/prescription-order';
import PrescriptionPage from './Home/prescriptions';
import ProductPage from './Home/product';
import React from 'react';
import ResetPasswordPage from './Auth/forgot-password/reset-password';
import RxPage from './Home/prescriptions/rx';
import SettingPage from './Home/settings';
import SignUpPage from './Auth/signup';
import { ToastContainer } from 'react-toastify';
import { User } from '../models/User';
import { isLoggedIn } from '../services/auth/auth.service';
import { profileActions } from '../store/Actions';
import { useNavigate } from "react-router-dom";

function App() {

  const dispatch = useAppDispatch();
  const [isLoaded, setIsLoaded] = useState(false);


  const isLogin = useAppSelector(state => state.profileReducer.isLogin);
  const user: User = useAppSelector(state => state.profileReducer.user);

  useEffect(() => {
    init()
  });

  useEffect(() => {
    if (isLogin) {
      try {
        const localUser = localStorage.getItem("User");
        if (localUser) {
          const user: User = User.create(JSON.parse(localUser)) as User;
          dispatch(profileActions.setUserData({ user }));
        } else {
          localStorage.clear();
          sessionStorage.clear();
          dispatch(profileActions.setLogin(false));
        }

        setIsLoaded(true);
      } catch (err) {
        localStorage.clear();
        sessionStorage.clear();
        dispatch(profileActions.setLogin(false));
        setIsLoaded(true);
      }
    } else {
      setIsLoaded(true);
    }

  }, [isLogin, dispatch])

  const init = () => {
    if (localStorage) {
      dispatch(profileActions.setLogin(isLoggedIn()));
    } else {
      setTimeout(() => {
        init();
      }, 100);
    }
  }

  const AuthRoutes = () => {
    const navigate = useNavigate();

    useEffect(() => {
      if (isLogin) {
        navigate('/home');
      }
    });
    return (
      <React.Fragment>
        {
          !isLogin && <Outlet />
        }
      </React.Fragment>
    );
  }

  const HomeRoutes = () => {
    const navigate = useNavigate();

    useEffect(() => {
      if (!isLogin) {
        navigate('/');
      }
    });
    return (
      <React.Fragment>
        {
          isLogin && <Outlet />
        }
      </React.Fragment>
    );
  }

  const HomeRedirect = () => {
    const navigate = useNavigate();
    useEffect(() => {
      if (user.isApproved) navigate('/home/dashboard');
      if (!user.isApproved) navigate('/home/product');
    });

    return (<></>)
  }

  return (
    isLoaded ?
      <>
        <Routes>
          <Route path="/" element={<AuthRoutes />} >
            <Route path="" element={<LoginPage />} />
            <Route path="signup" element={<SignUpPage />} />
            <Route path="forgot-password" element={<ForgotPassword />} />
            <Route path="forgot-password/done" element={<DonePage />} />
          </Route>

          <Route path="/home" element={<HomeRoutes />} >
            <Route path="" element={<HomeRedirect />} />
            <Route path="dashboard" element={<DashboardPage />} />
            <Route path="patient" element={<PatientPage />} />
            <Route path="prescriptions" element={<PrescriptionPage />} />
            {user.isApproved &&
              <Route>
                <Route path="patient/detail/:id" element={<Detail />} />
                <Route path="prescriptions/rx" element={<RxPage />} />
                <Route path="prescriptions/order/:id" element={<OrderPage />} />
                <Route path="prescription-order" element={<PrescriptionOrderPage />} />
              </Route>
            }
            <Route path="settings" element={<SettingPage />} />
            <Route path="product" element={<ProductPage />} />
            <Route path="reset-password" element={<ResetPasswordPage />} />
            <Route path="favorites" element={<FavouritePage />} />
          </Route>
        </Routes>
        <ToastContainer />
      </>
      : <div></div>
  );
}

export default App;

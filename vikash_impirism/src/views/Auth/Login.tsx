import { Box, Container, FormControlLabel, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";

import AuthService from "../../services/auth/login.service";
import BottomImage from "../../assets/icons/side-bottom.svg";
import ChatFloatingIcon from "../../components/chat-floating-icon";
import { CheckInput } from "../../core/forms/inputs/CheckInput";
import EmailInputIcon from "../../core/forms/inputs/EmailInputIcon";
import LogoImage from "../../assets/icons/logo.svg";
import PasswordInputIcon from "../../core/forms/inputs/PasswordInputIcon";
import PrimaryButton from "../../core/buttons/primary-button";
import ShadowImage from "../../assets/icons/login_shadow.svg";
import TopImage from "../../assets/icons/side-top.svg";
import { User } from "../../models/User";
import { profileActions } from "../../store/Actions";
import { useAppDispatch } from "../../store";
import { useNavigate } from "react-router-dom";

export const LoginPage = () => {

    const navigate = useNavigate();
    let auth: AuthService;

    const getController = () => {
        auth = new AuthService();
        return auth.loginForm;
    };

    const dispatch = useAppDispatch();
    const submit = async () => {
        let logged = await auth.loginHandler();
        if (logged) {
            dispatch(profileActions.setUserData({ user: logged as User }));
            navigate("/home");
        }
    };

    return (
        <Box className="page login-page d-vh-center">
            <Container maxWidth={false} className="login_outer">
                <Stack direction="column" className="login-card">
                    <Box className="logo-box d-vh-center">
                        <img src={LogoImage} alt="logo" width={320} />
                    </Box>

                    <Stack direction="column" className="login-form-box">
                        <Stack>
                            <Typography className="title">Prescriber Portal</Typography>
                        </Stack>
                        <Stack>
                            <Typography className="subtitle">Access your account to get a prescription online.</Typography>
                        </Stack>

                        <FieldGroup control={getController()} render={() => (
                            <form autoComplete="false">
                                <Stack className="form-fields dark">
                                    <Box mb={2}>
                                        <FieldControl name="email" render={EmailInputIcon} meta={{ label: "Email", placeholder: "Please Enter Email" }} />
                                    </Box>
                                    <Box>
                                        <FieldControl name="password" render={PasswordInputIcon} meta={{ label: "Password", placeholder: "Please Enter Password", type: "password" }} />
                                    </Box>
                                    <Box mt={2} mb={3}>
                                        <Stack direction={{ xs: "column", sm: "row" }} spacing={{ xs: 2, sm: 0 }} justifyContent="start" alignItems="start" className="checkbox_outer">
                                            <Box className="dark-checkbox">
                                                <FormControlLabel sx={{ margin: 0 }} control={<FieldControl name="rememberMe" render={CheckInput} />} label="Remember me" />
                                            </Box>
                                            <Box>
                                                <a href="/forgot-password" style={{ color: "#fff", textDecoration: "none" }}>
                                                    <Typography style={{ cursor: "pointer" }}>Forgot your Password?</Typography>
                                                </a>
                                            </Box>
                                        </Stack>
                                    </Box>
                                    <Box className="login-button-outer">
                                        <Stack direction={{ xs: "column", sm: "row" }} spacing={{ xs: 2, sm: 0 }} justifyContent="start" className="loginSubmitBtn">
                                            <Box className="loginSubmitBtnInfo">
                                                <PrimaryButton label={"LOG IN"} onClick={submit} />
                                            </Box>
                                            <Box className="loginSubmitBtnInfo">
                                                <a href="/signup" style={{ textDecoration: "none" }}>
                                                    <PrimaryButton label={"REGISTER"} />
                                                </a>
                                            </Box>
                                        </Stack>
                                    </Box>
                                </Stack>
                            </form>
                        )}
                        />
                    </Stack>
                    <Stack className="bottom_shadow" direction="row" justifyContent="center">
                        <img src={ShadowImage} alt="" width={700} />
                    </Stack>
                </Stack>

                <ChatFloatingIcon />
            </Container>

            <Box className="left_top_img">
                <img src={TopImage} alt="" />
            </Box>
            <Box className="right_bottom_img">
                <img src={BottomImage} alt="" />
            </Box>
        </Box>
    );
}
import { Model } from "./Model";

export class Product extends Model {
  icon = "";
  name = '';
  className = "Product";
  is_favourite = false;
  product_id = '';
  package_id = '';
  Child: Product[] = [];
  parent_id = '';
  medication_name = '';
  ndc ='';
  package_name= '';
  sub_pkg_id = '';
  Variations: {size: string}[] = [];

  get getImage() {
    return '';
  }
}

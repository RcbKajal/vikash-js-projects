import { ASSET_BASE_URL, LOCAL_PROFILE_ENV } from "../constants/Endpoints";

import { Model } from "./Model";

export class User extends Model {

  className = "User";

  profile_image_path = null;
  active = 0;
  first_name = '';
  last_name = '';
  doctor_id = '';
  mobile = '';
  password = '';
  email = '';
  md_id = '';
  npi = '';
  notification = '';
  Specialty = '';

  get getImage() {
    if (!this.profile_image_path) {
      return LOCAL_PROFILE_ENV + "assets/icons/dummy_user.png";
    } else {
      return ASSET_BASE_URL + this.profile_image_path;
    }
  }

  get isApproved() {
    return Number(this.active) !== 0;
  }
}

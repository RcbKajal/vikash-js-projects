import { Button, ButtonProps, styled } from "@mui/material";

const ColorButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: "#00ACBA",
  fontSize: 17,
  fontWeight: 600,
  backgroundColor: "transparent",
  border: "2px solid #00ACBA",
  borderRadius: "8px",
  boxShadow: "none",
  minWidth: 146,
  "&:hover": {
    backgroundColor: "#00ACBA",
    color: "#ffffff",
    boxShadow: 'none'
  },
  "&.MuiButton-root": {
    padding: 10,
  },
}));

const SecondaryButton = (props: {onClick?: ()=>void , label?: string}) => {
  return (
    <ColorButton onClick={props.onClick} className="primary-button" variant="contained" sx={{ width: "100%" }}>
      {props?.label}
    </ColorButton>
  );
};

export default SecondaryButton;

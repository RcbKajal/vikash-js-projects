import { FormArray, FormControl as FormControlType, FormGroup } from "react-reactive-form";
import { FormControl, InputAdornment, TextField } from "@mui/material";

import { Color } from "../../interfaces/Color";
import cvvIcon from "../../assets/icons/cvv.svg";

export const InputCvv =  ({ handler, touched, hasError, meta }: FormArray | FormControlType | FormGroup) => {

  const inputElem = handler();
  inputElem.value = meta.value ?? '';
  
  const getColor = () => {
    if (touched) {
      if (hasError("required")) {
        return Color.ERROR;
      }
      return Color.SUCCESS;
    }
    return Color.PRIMARY;
  };

  const getError = () => {
    if (touched) {
      if (hasError("required")) {
        return true;
      }
      return false;
    }

    return false;
  };

  return (
    <FormControl sx={{ width: "100%" }}>
      <TextField name={meta.name} type="password" InputProps={{ endAdornment: (<InputAdornment position="start"><img src={cvvIcon} alt="input_icon" width={15} /></InputAdornment>)}}
        color={getColor()} helperText={getError() && meta.helperText} required fullWidth label={meta.label} error={getError()} {...inputElem}/>
    </FormControl>
  );
};

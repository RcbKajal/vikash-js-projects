import { Paper, Stack } from "@mui/material";
import React, { Dispatch } from "react";
import { StyledTableCell, StyledTableRow } from "../../core/tables/tableStyles";

import Add from "../../assets/icons/add-icon.svg";
import { PatientInterface } from "../../interfaces/patient";
import { SortConfigInterface } from "../../interfaces/sortConfig";
import Table from "@mui/material/Table";
import TableArrow from "../../assets/icons/table_arrow.svg";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import moment from 'moment';
import { useNavigate } from "react-router-dom";

const useSortableData = (items: PatientInterface[] = [], config = null) => {
  const [sortConfig, setSortConfig]: [SortConfigInterface | null, Dispatch<SortConfigInterface| null>] = React.useState<SortConfigInterface | null>(config);

  const sortedItems = React.useMemo(() => {
    let sortableItems = items;
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key as keyof PatientInterface] < b[sortConfig.key as keyof PatientInterface]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key as keyof PatientInterface] > b[sortConfig.key as keyof PatientInterface]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key: string) => {
    let direction = "ascending";
    if (sortConfig && sortConfig.key === key && sortConfig.direction === "ascending") {
      direction = "descending";
    }
    setSortConfig({ key, direction });
  };

  return { items: sortedItems, requestSort, sortConfig };
};

export const PatientTable = (props: {
  data: PatientInterface[],
  onChange?: () => void;
}) => {
  const router = useNavigate();

  const formatDate = (date: string) => {
    if(!date)  {
      return '';
    }
    return moment(date).format('MM/DD/YYYY')
  }

  const handleContactProfile = (row: PatientInterface) => {
    router("/home/patient/detail/" + row.patId);
  };

  const { items, requestSort, sortConfig } = useSortableData(props.data ?? []);

  const getClassNamesFor = (name: string) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };
  
  return (
    <TableContainer component={Paper} className="table_customized_Prescription">
      <Table sx={{ minWidth: 700 }} stickyHeader>
        <TableHead className="table_head">
          <TableRow>
            <StyledTableCell onClick={() => requestSort("Patient_Name")} className={getClassNamesFor("Patient_Name")}>
              Patient Name{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("dob")} className={getClassNamesFor("dob")}>
              DOB{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("mobile")} className={getClassNamesFor("mobile")}>
              Phone{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("prescriber")} className={getClassNamesFor("prescriber")}>
              Prescriber{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell>Add Prescription</StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody className="table_body">
          {items && items.map((row: PatientInterface, index: number) => (
            <StyledTableRow key={index}>
              <StyledTableCell component="td" className="table_first" onClick={() => handleContactProfile(row)}>
                <Stack direction="row" alignItems="center" justifyContent="flex-start" style={{ paddingLeft: 15 }}>
                  <span className="table_profile_image">PM</span> {row.firstName} {row.lastName}
                </Stack>

              </StyledTableCell>
              <StyledTableCell component="td" onClick={() => handleContactProfile(row)}>
                <Stack>
                  <Stack>{formatDate(row.dateOfBirth)}</Stack>
                </Stack>
              </StyledTableCell>
              <StyledTableCell component="td" onClick={() => handleContactProfile(row)}>
                <Stack>{row.phoneNumber}</Stack>
              </StyledTableCell>
              <StyledTableCell component="td" onClick={() => handleContactProfile(row)}>
                <Stack></Stack>
              </StyledTableCell>
              <StyledTableCell component="td" onClick={props.onChange} className="plusIcon">
                <img src={Add} height={15} width={15} alt="logo" />
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

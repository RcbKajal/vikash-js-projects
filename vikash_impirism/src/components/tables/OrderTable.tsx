import { Box, Paper, Stack } from "@mui/material";
import React, { Dispatch } from "react";
import { StyledTableCell, StyledTableRow } from "../../core/tables/tableStyles";

import { OrderInterface } from "../../interfaces/order";
import { SortConfigInterface } from "../../interfaces/sortConfig";
import Table from "@mui/material/Table";
import TableArrow from "../../assets/icons/table_arrow.svg";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import moment from "moment";
import { useNavigate } from "react-router-dom";

const useSortableData = (items = [], config = null) => {
  const [sortConfig, setSortConfig]: [SortConfigInterface | null, Dispatch<SortConfigInterface | null>] = React.useState<SortConfigInterface | null>(config);

  const sortedItems = React.useMemo(() => {
    let sortableItems = [...items];
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key: string) => {
    let direction = "ascending";
    if (sortConfig && sortConfig.key === key && sortConfig.direction === "ascending") {
      direction = "descending";
    }
    setSortConfig({ key, direction });
  };

  return { items: sortedItems, requestSort, sortConfig };
};

export const OrderTable = (props: {
  data? :[]
}) => {
  const router = useNavigate();
  const handleRxNumber = () => {
    router("/home/prescriptions/rx");
  };

  const handleOrderNumber = (Order_Number: string) => {
    router("/home/prescriptions/order/" + Order_Number);
  };

  const formatDate = (date: string) => {
    return moment(date).format('MM/DD/YYYY')
  }
 
  const { items, requestSort, sortConfig } = useSortableData(props.data ?? []);

  const getClassNamesFor = (name: string) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };
  return (
    <TableContainer sx={{ maxHeight: 200 }} component={Paper} className="table_customized_Prescription">
      <Table sx={{ minWidth: 700 }} stickyHeader>
        <TableHead className="table_head">
          <TableRow>
            <StyledTableCell onClick={() => requestSort("Rx_Number")} className={getClassNamesFor("Rx_Number")}>
              RX Number{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Order_Number")} className={getClassNamesFor("Order_Number")}>
              Order ID{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Patient_Name")} className={getClassNamesFor("Patient_Name")}>
              Patient{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Prescriber_Name")} className={getClassNamesFor("Prescriber_Name")}>
              Prescriber{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Status")} className={getClassNamesFor("Status")}>
              Status{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Tracking")} className={getClassNamesFor("Tracking")}>
              Tracking
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Medication")} className={getClassNamesFor("Medication")}>
              Medication{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
            <StyledTableCell onClick={() => requestSort("Issue_Date")} className={getClassNamesFor("Issue_Date")}>
              Issue Date{" "}
              <span>
                <img className="right_arrow" src={TableArrow} alt="logo" height={10} width={10} />
              </span>
            </StyledTableCell>
          </TableRow>
        </TableHead>

        <TableBody className="table_body">
          {items && items.map((row: OrderInterface, index: number) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="td" className="table_first" onClick={handleRxNumber}>
                  <Stack>
                    <Box className="table_first">RX-{row.Rx_Number}</Box>
                  </Stack>
                </StyledTableCell>
                <StyledTableCell component="td" className="table_first" onClick={() => handleOrderNumber(row.Order_Number)}>
                  <Stack>
                    <Box className="table_first">ORD-{row.Order_Number}</Box>
                  </Stack>
                </StyledTableCell>
                <StyledTableCell component="td">
                <Stack direction="row" alignItems="center" justifyContent="flex-start" style={{paddingLeft:15}}>
                    <span className="table_profile_image">PM</span> {row.Patient_Name}
                  </Stack>
                </StyledTableCell>
                <StyledTableCell component="td">
                  <Stack>{row.Prescriber_Name}</Stack>
                </StyledTableCell>
                <StyledTableCell component="td">
                  <Stack>{row.Status}</Stack>
                </StyledTableCell>
                <StyledTableCell component="td" className="table_first">
                  <Stack>{row.Tracking}</Stack>
                </StyledTableCell>
                <StyledTableCell component="td">
                  <Stack>{row.Medication}</Stack>
                </StyledTableCell>
                <StyledTableCell component="td">
                  <Stack>{formatDate(row.Issue_Date)}</Stack>
                </StyledTableCell>
              </StyledTableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

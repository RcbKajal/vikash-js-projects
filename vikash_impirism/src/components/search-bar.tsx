import * as React from "react";

import {Box, CircularProgress, FormControl, TextField} from "@mui/material";

import searchIcon from "../../src/assets/icons/search_icon.svg";
import { ChangeEvent } from "react";

const SearchBar = (props:{ value:string, setValue:( e: ChangeEvent<HTMLInputElement>) => void}) => {

  const loading = false;

  return (
    <Box className="search-bar-autocomplete">
      <FormControl className="autocomplete form-input" sx={{ width: "100%" }}>
        <TextField label="Type Here to Search..." variant="outlined" value={props.value} onChange={props.setValue} InputProps={{
          endAdornment: (
            <React.Fragment>
              {loading ? <CircularProgress color="inherit" size={20} /> : <img src={searchIcon} alt="search" height={14} width={18} />}
            </React.Fragment>
          )
        }} />
      </FormControl>
    </Box>
  );
};

export default SearchBar;

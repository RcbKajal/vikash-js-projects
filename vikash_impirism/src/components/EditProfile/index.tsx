import * as React from "react";

import { Box, Container, FormControl, FormControlLabel, Grid, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";

import { AddNewCard } from "../popup/AddNewCard";
import { BootstrapDialog } from "../../core/tables/tableStyles";
import { InputDate } from "../../core/forms/inputDate";
import { InputEmail } from "../../core/forms/InputEmail";
import { InputNumber } from "../../core/forms/inputs/InputNumber";
import { InputSelect } from "../../core/forms/inputs/InputSelect";
import { InputText } from "../../core/forms/inputs/InputText";
import User_icon from "../../assets/icons/user.svg";
import Volate from "../../assets/icons/volate.svg";
import deleteIcon from "../../assets/icons/deleteIcon.svg";
import { genderOptions } from "../../services/components/selectOptions.service";
import { personalUserForm } from "../../services/pages/settings/personalLoginForm";
import { useState } from "react";

export default function EditProfile() {


  const [data, setData] = useState({
    card: false,
  });
  const handleNewCard = () => {
    setData((oldData) => ({
      ...oldData,
      card: true,
    }));
  };

  const handleClose = () => {
    setData((oldData) => ({
      ...oldData,
      card: false,
    }));
  };
  return (
    <>
      {data.card && (
        <BootstrapDialog onClose={handleClose} open={data.card} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
          <AddNewCard handleClose={handleClose} />
        </BootstrapDialog>
      )}
      <Box className="contact_profile">
        <Container maxWidth="xl">
          <Stack className="Contact_img" display="flex" alignItems="center" justifyContent="center">
            <Box className="tw_icon_out tw_icon_out_padding">
              <span className="tw_icon">TW</span>
              Theresa Woodman
            </Box>
          </Stack>
          <Grid container spacing={2} padding={0} className="main_info_alignment">
            <Grid className="details_box details_border" xs={12} sm={12} md={12} lg={7} p="0">
              <Box className="personal_details" mb={4}>
                <Typography className="heading">
                  <span className="profile_icon">
                    <img src={User_icon} alt="Imprimis RX A Harrow Company" width={16} />
                  </span>
                  PERSONAL INFO
                </Typography>
              </Box>
              <FieldGroup
                control={personalUserForm}
                render={({ get, invalid }) => (
                  <form>
                    <Grid item xs={12} lg={12}>
                      <Grid container spacing={1} className="contact_outer">
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">Office / Residential Phone</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="res_phone" render={InputText} meta={{ name: "res_phone", value: "res_phone", helperText: "Office / Residential Phone is required", label: "Office / Residential Phone", placeholder: "Please Enter Office / Residential Phone" }} />
                          </Grid>
                        </Stack>
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">Mobile Phone</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="phone" render={InputNumber} meta={{ name: "phone", value: "phone", helperText: "Mobile Phone is required", label: "Mobile Phone", placeholder: "Please Enter Mobile Phone" }} />
                          </Grid>
                        </Stack>
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">Email</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="email" render={InputEmail} meta={{ name: "email", value: "email", helperText: "Email is Required.", label: "Email", placeholder: "Please Enter Email" }} />
                          </Grid>
                        </Stack>
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">DOB</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="dob" render={InputDate} meta={{ name: "dob", value: "dob", helperText: "DOB is required", label: "DOB", placeholder: "Please Enter DOB" }} />
                          </Grid>
                        </Stack>
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">Gender</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="gender" render={InputSelect} meta={{ name: "gender", value: "gender", options: genderOptions, label: "Gender", placeholder: "Please Enter Gender" }} />
                          </Grid>
                        </Stack>
                        <Stack className="contact" direction="row" alignItems="center">
                          <Box className="contact_side">Shipping Address</Box>
                          <Grid item xs={12} sm={12} md={12} lg={7} className="alignment">
                            <FieldControl name="shpping_add" render={InputText} meta={{ name: "shpping_add", value: "shpping_add", helperText: "Shipping Address is required", label: "Shipping Address", placeholder: "Please Enter Shipping Address" }} />
                          </Grid>
                        </Stack>
                      </Grid>
                    </Grid>
                  </form>
                )}
              />
            </Grid>
            <Grid className="details_box" xs={12} sm={12} md={12} lg={5} paddingLeft={{ xs: 0, md: 0, lg: 0 }}>
              <Box className="payment payment_prescriber">
                <Box className="personal_details" mb={4}>
                  <Typography className="heading">
                    <span className="profile_icon">
                      <img src={Volate} alt="Imprimis RX A Harrow Company" width={16} />
                    </span>
                    BILLING INFO
                  </Typography>
                </Box>
                <Stack direction="row" justifyContent="flex-start" alignItems="center" className="top_alignment">
                  {/* <Grid container xs={12} sm={12} md={12} lg={12}> */}
                  <Box className="exeasting gap_top">
                    <Typography component="h2">Existing Cards</Typography>

                    <FormControl>
                      <RadioGroup defaultValue="female" className="radio_grid" aria-labelledby="demo-customized-radios" name="customized-radios">
                        <Stack direction="row" alignItems="center">
                          <FormControlLabel value="female" control={<Radio />} label="American Express - x4458" />
                          <Stack>
                            <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                          </Stack>
                        </Stack>
                        <Stack direction="row" alignItems="center">
                          <FormControlLabel value="male" control={<Radio />} label="Mastercard - x8597" />
                          <Stack>
                            <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                          </Stack>
                        </Stack>
                      </RadioGroup>
                    </FormControl>
                    <Box className="add_new_cart gap_top">
                      <button className="edit_btn" onClick={handleNewCard} style={{color: "#00ACBA", fontSize: "18px", fontWeight: "700", backgroundColor: "#fff",
                          border: "2px solid #00ACBA", borderRadius: "8px", boxShadow: "none", height: "46px", width: "198", textTransform: "capitalize"}}>
                        Add New Card
                      </button>
                    </Box>
                  </Box>
                  {/* </Grid> */}
                </Stack>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
}

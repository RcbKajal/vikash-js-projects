import { Box, Container, FormControl, FormControlLabel, Grid, Radio, RadioGroup, Stack, Typography } from "@mui/material";

import { AddNewCard } from "../popup/AddNewCard";
import { BootstrapDialog } from "../../core/tables/tableStyles";
import { PatientInterface } from "../../interfaces/patient";
import User from "../../assets/icons/user.svg";
import Volate from "../../assets/icons/volate.svg";
import deleteIcon from "../../assets/icons/deleteIcon.svg";
import { useState } from "react";

const ContactProfile = (props: {profile: PatientInterface}) => {
  
  const [data, setData] = useState({
    card: false,
    prescriber: false,
    address: false,
    edit: false,
    addPrescriber: false,
  });

  const handleNewCard = () => {
    setData((oldData) => ({
      ...oldData,
      card: true,
    }));
  };

  const handleClose = () => {
    setData((oldData) => ({
      ...oldData,
      card: false,
      prescriber: false,
      address: false,
      edit: false,
      addPrescriber: false,
    }));
  };
  return (
    <>
      {data.card && (
        <BootstrapDialog onClose={handleClose} open={data.card} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
          <AddNewCard handleClose={handleClose} />
        </BootstrapDialog>
      )}
      <Box className="contact_profile">
        <Container maxWidth="xl">
          <Stack className="Contact_img" display="flex" alignItems="center" justifyContent="center">
            <Box className="tw_icon_out">
              <span className="tw_icon">TW</span>
              {props.profile?.fname} {props.profile?.lname}
            </Box>
          </Stack>
          <Grid container spacing={2}>
            <Grid item className="details_box details_border" sm={12} md={12} lg={7} p="0">
              <Box className="personal_details" mb={4}>
                <Typography className="heading">
                  <span className="profile_icon">
                    <img src={User} alt="Imprimis RX A Harrow Company" width={16} />
                  </span>
                  PERSONAL INFO
                </Typography>
              </Box>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">Office / Residential Phone</Box>
                <Box className="info_contact">{props.profile?.area_code} {props.profile?.phone_no}</Box>
              </Stack>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">Mobile Phone</Box>
                <Box className="info_contact">{props.profile?.area_code} {props.profile?.phone_no}</Box>
              </Stack>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">Email</Box>
                <Box className="info_contact">{props.profile?.email}</Box>
              </Stack>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">DOB</Box>
                <Box className="info_contact">{props.profile?.birth_date}</Box>
              </Stack>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">Gender</Box>
                <Box className="info_contact">{props.profile?.gender_cd}</Box>
              </Stack>
              <Stack className="contact" direction="row" alignItems="center">
                <Box className="contact_side">Shipping Address</Box>
                <Box className="info_contact long">{props.profile?.addr1} {props.profile?.addr2} {props.profile?.state_cd} {props.profile?.zip}</Box>
              </Stack>
            </Grid>
            <Grid item className="details_box" xs={12} sm={12} md={12} lg={5} paddingLeft={{ xs: 0, md: 0, lg: 0 }}>          
                <Box className="payment payment_prescriber">
                 
                    <Box className="personal_details" mb={4}>
                      <Typography className="heading">
                        <span className="profile_icon">
                          <img src={Volate} alt="Imprimis RX A Harrow Company" width={16} />
                        </span>
                        BILLING INFO
                      </Typography>
                    </Box>
                    <Stack direction="row" justifyContent="flex-start" alignItems="center" className="top_alignment">
                        <Box className="exeasting gap_top">
                          <Typography component="h2">Existing Cards</Typography>

                          <FormControl>
                            <RadioGroup defaultValue="female" className="radio_grid" aria-labelledby="demo-customized-radios" name="customized-radios">
                              <Stack direction="row" alignItems="center">
                                <FormControlLabel value="female" control={<Radio />} label="American Express - x4458" />
                                <Stack>
                                  <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                </Stack>
                              </Stack>
                              <Stack direction="row" alignItems="center">
                                <FormControlLabel value="male" control={<Radio />} label="Mastercard - x8597" />
                                <Stack>
                                  <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                </Stack>
                              </Stack>
                            </RadioGroup>
                          </FormControl>
                          <Box className="add_new_cart gap_top">
                            <button
                              className="edit_btn"
                              onClick={handleNewCard}
                              style={{
                                color: "#00ACBA",
                                fontSize: "18px",
                                fontWeight: "700",
                                backgroundColor: "#fff",
                                border: "2px solid #00ACBA",
                                borderRadius: "8px",
                                boxShadow: "none",
                                height: "46px",
                                width: "198",
                                textTransform: "capitalize",
                              }}
                            >
                              Add New Card
                            </button>
                          </Box>
                        </Box>
                    </Stack>
                 
                </Box>
              
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export default ContactProfile;

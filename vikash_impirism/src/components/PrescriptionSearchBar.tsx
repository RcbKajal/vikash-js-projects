import { Autocomplete, Box, InputAdornment, TextField } from "@mui/material";

import { Option } from "../models/Option";
import searchIcon from "../../src/assets/icons/search_icon.svg";

export const PrescriptionSearchBar = (props: { options?: Option[], filter?:(name: {label: string, value: string}) => {label: string, value: string}[] }) => {
  const defaultProps = {
    options: [],
    getOptionLabel: (option: Option) => option.label,
  };

  return (
    <>
      <Box className="search-bar-autocompletes">
        <Autocomplete {...defaultProps} clearOnEscape    filterOptions={(options, state) => {
            if (state.inputValue.length >= 1) {
              return options.filter((item: {label: string, value: string}) => {
                if (props.filter) {
                  return props.filter(item);
                }
              });
            }
            return [];
          }}

          renderInput={(params) => <TextField className="search_input" {...params} InputProps={<InputAdornment position="end"></InputAdornment>} variant="standard"/>}
        />
        <span className="search-bar-btn">
          <img src={searchIcon} alt="search" height={14} width={18} />
        </span>
      </Box>
    </>
  );
}
import { AppBar, Box, Container, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Stack, Typography } from "@mui/material";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { RootState, useAppDispatch, useAppSelector } from "../../store";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

import Dot from "../../assets/icons/top_dot.svg";
import MenuItem from "@mui/material/MenuItem";
import SwipeableTemporaryDrawer from "../swipedrawer/drawer";
import Uparrow from "../../assets/icons/pol.svg";
import { User } from "../../models/User";
import cartIconRX from "../../assets/icons/cart-rx.svg";
import chevronright from "../../assets/icons/chevron-right.svg";
import gear from "../../assets/icons/gear.svg";
import logoUrl from "../../assets/icons/logo.svg";
import logout from "../../assets/icons/logout.svg";
import { profileActions } from "../../store/Actions";
import starfill from "../../assets/icons/star-fill.svg";
import { toast } from "react-toastify";

const Header = () => {

  const router = useNavigate();

  const [open, setOpen] = useState(false);
  const [openSidebar, setOpenSidebar] = useState(false);
  const dispatch = useAppDispatch();

  const user: User = useAppSelector(state => state.profileReducer.user);

  const handleClick = () => {
    if (document.body.clientWidth > 900) {
      setOpenSidebar(false);
      setOpen(!open);
    } else {
      setOpenSidebar(!openSidebar);
      setOpen(false);
    }
  };

  const handleClose = () => {
    setOpen(false);
    setOpenSidebar(false);
  };

  const logoutHandler = async () => {
    try {
      await localStorage.removeItem("api_key");
      await localStorage.removeItem("isLoggedIn");
      await localStorage.removeItem("User");
      dispatch(profileActions.setLogin(false));
      router("/");
    } catch (err) {
      console.error(err);
      toast("Unable to remove Login details from device storage");
    }
  };

  const gotoDashboard = () => {
    router("/home/dashboard");
  };

  const settingHandler = () => {
    router("/home/settings");
  };

  return (
    <>
      <AppBar position="absolute" className="site-header">
        <Container maxWidth="xl">
          <Stack direction="row" alignItems="center" justifyContent="space-between">
            <Box>
              <Stack direction="row" alignItems="center">
                <Box className="navbar-brand">
                  <img src={logoUrl} alt="Imprimis RX A Harrow Company" width={150} onClick={gotoDashboard} />
                </Box>
                <Box className="navbar-nav">
                  <ul>
                    {user && <>
                      <li>
                        <NavLink to="/home/dashboard" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <div>
                            <img src={Dot} alt="Imprimis RX A Harrow Company" />
                          </div>
                          {user.isApproved ? 'Dashboard' : '-'}
                        </NavLink>
                      </li>

                      <li>
                        <NavLink to="/home/patient" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <div>
                            <img src={Dot} alt="Imprimis RX A Harrow Company" />
                          </div>
                          {user.isApproved ? 'Patients' : '-'}
                        </NavLink>
                      </li>
                      <li>
                        <NavLink to="/home/prescriptions" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <div>
                            <img src={Dot} alt="Imprimis RX A Harrow Company" />
                          </div>
                          {user.isApproved ? 'Prescriptions' : '-'}
                        </NavLink>
                      </li>
                    </>}
                    <li>
                      <NavLink to="/home/settings" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                        <div>
                          <img src={Dot} alt="Imprimis RX A Harrow Company" />
                        </div>
                        Settings
                      </NavLink>
                    </li>
                    <li>
                      <NavLink to="/home/product" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                        <div>
                          <img src={Dot} alt="Imprimis RX A Harrow Company" />
                        </div>
                        Product Catalog
                      </NavLink>
                    </li>
                  </ul>
                </Box>
              </Stack>
            </Box>
            <Box className="site-header-profile-side">
              <Stack className="site-header-profile-side" direction="row" alignItems="center" gap={3}>
                <Box className="user_welcome_name">
                  <Typography>
                    Welcome, <span>{user?.first_name + " " + user?.last_name}</span>
                  </Typography>
                </Box>
                <Box className="user_profile_head_pic" onClick={handleClick}>
                  <Box className="profile_pic_block" style={(user.profile_image_path) ? { background: `url('https://mobileauth.imprimisrx.com/development/webservices/images/originals/${user.profile_image_path}')` } : {}}></Box>
                </Box>
              </Stack>
              {open && (
                <Box className="swipeable-right-drawer">
                  <Box maxWidth="400px" width="300px" height="117px" className="user-profile-menu" display="flex" flexDirection="column">
                    <Box id="basic-menu">
                      <MenuItem>
                        <Stack direction="row" justifyContent="space-between" align-items="center" className="dashboard_dropdown" onClick={settingHandler}>
                          <Stack direction="row">Profile Settings</Stack>
                          <Stack direction="row">
                            <IconButton aria-label="Example" className="logout-sidebar-btn">
                              <img src={gear} alt="Imprimis RX A Harrow Company" />
                            </IconButton>
                          </Stack>
                        </Stack>
                      </MenuItem>
                      <MenuItem>
                        <Stack direction="row" justifyContent="space-between" align-items="center" className="dashboard_dropdown" onClick={logoutHandler}>
                          <Stack direction="row">Sign Out</Stack>
                          <Stack direction="row">
                            <IconButton aria-label="Example" className="logout-sidebar-btn">
                              <img src={logout} alt="Imprimis RX A Harrow Company" />
                            </IconButton>
                          </Stack>
                        </Stack>
                      </MenuItem>
                    </Box>
                    <div className="arrow_icon">
                      <img src={Uparrow} height={22} width={22} alt="logo" />
                    </div>
                  </Box>
                </Box>
              )}
            </Box>
          </Stack>
        </Container>
      </AppBar>

      {openSidebar && (
        <Box className="swipeable-right-drawer" overflow="clip">
          <SwipeableTemporaryDrawer open={openSidebar} onClose={handleClose} onOpen={() => { }}>
            <Box maxWidth="400px" width="300px" height="100%" className="sidebar-drawer-wrap" display="flex" flexDirection="column">
              <IconButton aria-label="Example" onClick={handleClose} className="close-sidebar-btn">
                <img src={chevronright} alt="Imprimis RX A Harrow Company" />
              </IconButton>
              <List className="sidebar-drawer-list">
                <ListItem>
                  <ListItemButton>
                    <ListItemText>My Favorites</ListItemText>
                    <ListItemIcon>
                      <img src={starfill} alt="Imprimis RX A Harrow Company" />
                    </ListItemIcon>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <ListItemText>View Cart</ListItemText>
                    <ListItemIcon className="cart-header-rx">
                      <img src={cartIconRX} alt="Imprimis RX A Harrow Company" /> <span>0</span>
                    </ListItemIcon>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <ListItemText>Profile Settings</ListItemText>
                    <ListItemIcon>
                      <img src={gear} alt="Imprimis RX A Harrow Company" />
                    </ListItemIcon>
                  </ListItemButton>
                </ListItem>
                <Box className="addon_small_screen">
                  <ul>
                    <ListItem>
                      <ListItemButton className="sidebar_btn">
                        <NavLink to="/home/dashboard" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <ListItemText>Dashboard</ListItemText>
                          <ListItemIcon>
                            <img src={gear} alt="Imprimis RX A Harrow Company" />
                          </ListItemIcon>
                        </NavLink>
                      </ListItemButton>
                    </ListItem>
                    <ListItem>
                      <ListItemButton className="sidebar_btn">
                        <NavLink to="/home/patient" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <ListItemText>patients</ListItemText>
                          <ListItemIcon>
                            <img src={gear} alt="Imprimis RX A Harrow Company" />
                          </ListItemIcon>
                        </NavLink>
                      </ListItemButton>
                    </ListItem>
                    <ListItem>
                      <ListItemButton className="sidebar_btn">
                        <NavLink to="/home/prescriptions" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <ListItemText>prescriptions</ListItemText>
                          <ListItemIcon>
                            <img src={gear} alt="Imprimis RX A Harrow Company" />
                          </ListItemIcon>
                        </NavLink>
                      </ListItemButton>
                    </ListItem>
                    <ListItem>
                      <ListItemButton className="sidebar_btn">
                        <NavLink to="/home/settings" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <ListItemText>settings</ListItemText>
                          <ListItemIcon>
                            <img src={gear} alt="Imprimis RX A Harrow Company" />
                          </ListItemIcon>
                        </NavLink>
                      </ListItemButton>
                    </ListItem>
                    <ListItem>
                      <ListItemButton className="sidebar_btn">
                        <NavLink to="/home/product" className={({ isActive }) => isActive ? "active && navbar-link" : "navbar-link "}>
                          <ListItemText>product catalog</ListItemText>
                          <ListItemIcon>
                            <img src={gear} alt="Imprimis RX A Harrow Company" />
                          </ListItemIcon>
                        </NavLink>
                      </ListItemButton>
                    </ListItem>
                  </ul>
                </Box>
              </List>
              <Stack direction="row" alignItems="center" className="sidebar-profile-block">
                <IconButton aria-label="Example" onClick={logoutHandler} className="logout-sidebar-btn">
                  <img src={logout} alt="Imprimis RX A Harrow Company" />
                </IconButton>

                <Box className="sidebar-profile-details" flex="1">
                  <Stack direction="row" alignItems="center" justifyContent="flex-end">
                    <Box className="sidebar-profile-desc" textAlign="right">
                      <Typography variant="h5" component="h5">
                        {user?.first_name + " " + user?.last_name}
                      </Typography>
                      <Typography>doctor@agoodman.com</Typography>
                    </Box>
                    <Box className="user_profile_head_pic">
                      <Box className="profile_pic_block" style={(user.profile_image_path) ? { background: `url('https://mobileauth.imprimisrx.com/development/webservices/images/originals/${user.profile_image_path}')` } : {}}></Box>
                    </Box>
                  </Stack>
                </Box>
              </Stack>
            </Box>
          </SwipeableTemporaryDrawer>
        </Box>
      )}
    </>
  );
};

export default Header;

import { AddNewPrescriberFormHandler, addNewPrescriberForm } from "../../../services/pages/popup-form/AddNewPrescriberForm.service";
import { Box, Container, DialogContent, FormControlLabel, Grid, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";

import { CheckInput } from "../../../core/forms/inputs/CheckInput";
import { InputEmail } from "../../../core/forms/InputEmail";
import { InputNumber } from "../../../core/forms/inputs/InputNumber";
import { InputText } from "../../../core/forms/inputs/InputText";
import PrimaryButton from "../../../core/buttons/primary-button";
import TertiaryButton from "../../../core/buttons/tertiary-button";
import { useAppSelector } from "../../../store";
import { useNavigate } from "react-router-dom";

export const AddPrescriber = (props: { handleClose: () => void }) => {
  const router = useNavigate();
  const userData = useAppSelector(state=> state.profileReducer.user)
  const handleAddNewPrescriber = () => {
    AddNewPrescriberFormHandler(router, props, userData);
  };
  return (
    <>
      <DialogContent dividers className="popup_content">
        <Box component="main" className="card-info">
          <Container maxWidth="lg">
            <Box className="main-box">
              <Box sx={{ bgcolor: "background.paper" }}>
                <Stack className="modal_heading_main" direction="row" justifyContent="center" alignItems="center" pt={3}>
                  <Typography className="heading_bottom_without_border">Add New Prescriber</Typography>
                </Stack>
                <Box>
                  <FieldGroup control={addNewPrescriberForm} render={({ get, invalid }) => {
                   
                    return (
                      <form>

                        <Grid container spacing={2}>
                          <Grid item xs={12} md={12} lg={12}>
                            <FieldControl name="firstName" render={InputText} meta={{ name: "firstName", value: "firstName", helperText: "Prescriber First Name is required", label: "Prescriber First Name", placeholder: "Please Enter Prescriber First Name" }} />
                          </Grid>
                          <Grid item xs={12} md={12} lg={12}>
                            <FieldControl name="lastName" render={InputText} meta={{ name: "lastName", value: "lastName", helperText: "Prescriber Last Name is required", label: "Prescriber Last Name", placeholder: "Please Enter Prescriber Last Name" }} />
                          </Grid>

                          <Grid item xs={12} md={12} lg={12}>
                            <FieldControl name="phone" render={InputNumber} meta={{ name: "phone", value: "phone", helperText: "Prescriber Phone Number is required", label: "Prescriber Phone", placeholder: "Please Enter Phone Number" }} />
                          </Grid>

                          <Grid item xs={12} sm={12} md={4} lg={4}>
                            <FieldControl name="email" render={InputEmail} meta={{ name: "email", value: "email", helperText: "Prescriber Email is Required.", label: "Prescriber Email", placeholder: "Please Enter Email" }} />
                          </Grid>

                          {get('donot_npi')?.value && <Grid item xs={12} sm={12} md={4} lg={4}>
                            <FieldControl name="npi" render={InputText} meta={{ name: "npi", value: "npi", helperText: "Npi is required", label: "NPI", placeholder: "Please Enter NPI", required: false }} />
                          </Grid>}

                          {!get('donot_npi')?.value && <Grid item xs={12} sm={12} md={4} lg={4}>
                            <FieldControl name="npi" render={InputText} meta={{ name: "npi", value: "npi", helperText: "Npi is required", label: "NPI", placeholder: "Please Enter NPI" }} />
                          </Grid>}
                          <Grid item xs={12} sm={12} md={4} lg={4}>
                            <Stack direction={"row"} gap={2} mt={2} mb={4}>
                              <FormControlLabel sx={{ margin: 0 }} className="check-input-with-label" control={<FieldControl name="donot_npi" render={CheckInput} />} label="I do not have a NPI" />
                            </Stack>
                          </Grid>

                          <Grid item xs={12} sm={12} md={12} lg={12} mt={3}>
                            <Stack className="add_btnouter" direction="row" justifyContent="center" alignItems="center" gap={1.5}>
                              <Box className="add_outerbtnscancel">
                                <TertiaryButton label={'Cancel'}  onClick={() => props.handleClose()}/>
                              </Box>
                              <Box className="add_outerbtns">
                                <PrimaryButton label={'Add prescriber'}  onClick={() => handleAddNewPrescriber()} />
                              </Box>
                            </Stack>
                          </Grid>
                        </Grid>
                      </form>
                    )
                  }}
                  />
                </Box>
              </Box>
            </Box>
          </Container>
        </Box>
      </DialogContent>
    </>
  );
};

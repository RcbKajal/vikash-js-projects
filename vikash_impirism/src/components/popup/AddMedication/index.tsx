import { AddMedicationForm, AddMedicationHandler } from "../../../services/pages/popup-form/AddMedicationForm.service";
import { Box, Container, DialogContent, DialogTitle, Grid, IconButton, Stack, Typography } from "@mui/material";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { useAppDispatch, useAppSelector } from "../../../store";

import CloseIcon from "@mui/icons-material/Close";
import { DialogTitleProps } from "../../../interfaces/DialogTitleProps";
import { InputComment } from "../../../core/forms/InputComment";
import { InputSearchSelect } from "../../../core/forms/inputs/InputSearchSelect";
import { InputSelect } from "../../../core/forms/inputs/InputSelect";
import { InputText } from "../../../core/forms/inputs/InputText";
import { PRODUCT_CATALOG } from "../../../constants/Endpoints";
import PhysicianDoctor from "../../../constants/api";
import PrimaryButton from "../../../core/buttons/primary-button";
import { Product } from "../../../models/Product";
import { ProductSize } from "../../../models/ProductSize";
import TertiaryButton from "../../../core/buttons/tertiary-button";
import { productActions } from "../../../store/Actions";

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton onClick={onClose} sx={{ position: "absolute", right: 8, top: 8, color: (theme) => theme.palette.grey[500] }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export const AddMedication = (props: { handleClose: () => void }) => {

  const dispatch = useAppDispatch();
  const ProductCatalogs = useAppSelector(state => state.productReducer.products);

  const [sizes, setSizes]: [ProductSize[], Dispatch<SetStateAction<ProductSize[]>>] = useState<ProductSize[]>([]);

  const loadCatalog = async () => {
    try {
      const result = await PhysicianDoctor.post(PRODUCT_CATALOG, {}, { params: { api_key: localStorage.getItem("api_key") } })
      const productData = result.data.response;
      if (productData !== null) {
        dispatch(productActions.setProductCatalogData({ data: productData }));
      }
    } catch (err) {}   
  }

  useEffect(() => {
    AddMedicationForm.reset();
    loadCatalog();
  }, [dispatch, loadCatalog]);

  const onSelectSize = (e: React.SyntheticEvent) => {
    const index: number = Number((e.target as HTMLInputElement).value);
    AddMedicationForm.patchValue({
      quantity: sizes[index].quantity,
      supply: sizes[index].day_supply,
      refills: sizes[index].refills,
      comments: '',
    })
  }

  const onSelectMedication = (value: {data: Product}) => {
    const selected = ProductCatalogs.find((item:Product) => item.product_id === value.data.product_id);
    setSizes(selected.Variations);
    AddMedicationForm.patchValue({
      size: '0',
      quantity: selected.Variations[0].quantity,
      supply: selected.Variations[0].day_supply,
      refills: selected.Variations[0].refills,
      choose_dosing_instruction: selected.sig_dosing_instruction,
      choose_medical_necessity: selected.medical_necessity,
      comments: '',
    })
  };

  const addMedicationHandler = () => {
    AddMedicationHandler(props, dispatch)
  }

  return (
    <>
      <BootstrapDialogTitle id="customized-dialog-title" onClose={props.handleClose}></BootstrapDialogTitle>
      <DialogContent dividers className="popup_content_medication">
        <Box component="main" className="popup-info_medication">
          <Container maxWidth="lg">
            <Box className="main-box">
              <Box sx={{ bgcolor: "background.paper" }}>
                <Stack className="modal_heading_main" direction="row" justifyContent="center" alignItems="center">
                  <Typography className="heading_bottom_without_border">Add Medication</Typography>
                </Stack>
                <Box>
                  {ProductCatalogs &&
                    <FieldGroup control={AddMedicationForm} render={({ get, invalid }) => (
                      <form>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="medication" render={InputSearchSelect} meta={{
                              name: "medication", options: ProductCatalogs.map((item: Product) => {
                                return { label: item.medication_name, value: item.product_id, data: item };
                              }), label: "Medication", placeholder: "Please Enter Medication", onChange: onSelectMedication}} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="size" render={InputSelect} meta={{
                              name: "size", options: sizes.map((item:  ProductSize, index: number) => {
                                return { label: item.size, value: index, data: item };
                              }), label: "Quantity", placeholder: "Quantity", onChange: onSelectSize
                            }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="supply" render={InputText} meta={{ name: "supply", label: "Day Supply (Edit As Needed)", placeholder: "Please Enter Day Supply (Edit As Needed)" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="refills" render={InputSelect} meta={{
                              name: "refills", label: "Refills", placeholder: "Please Enter Refills", options: [
                                { label: 0, value: 0 },
                                { label: 1, value: 1 },
                                { label: 2, value: 2 },
                                { label: 3, value: 3 },
                                { label: 4, value: 4 },
                                { label: 5, value: 5 },
                                { label: 6, value: 6 },
                                { label: 7, value: 7 },
                                { label: 8, value: 8 },
                                { label: 9, value: 9 },
                                { label: 10, value: 10 },
                                { label: 11, value: 11 },
                              ]
                            }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="choose_dosing_instruction" render={InputText} meta={{ name: "choose_dosing_instruction", label: "Choose Dosing Instructions", placeholder: "Please Enter Choose Dosing Instructions" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="choose_medical_necessity" render={InputText} meta={{ name: "choose_medical_necessity", label: "Choose Medical Necessity", placeholder: "Please Enter Choose Medical Necessity" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="comments" render={InputComment} meta={{ name: "comments", value: "comments", label: "Comments", helperText: "Comments is required", placeholder: "Write Comments........." }} />
                          </Grid>
                          <Grid className="billing_info bottom" pt="0" container spacing={2}>
                            <Grid item xs={12} sm={12} md={12} lg={12} pl={0}>
                              <Grid item className="inputs_fields_ratio">
                                <Box className="mandatory">
                                  <span> * </span> This field is mandatory
                                </Box>

                                <Stack direction={'row'} gap={2}>
                                  <Box>
                                    <TertiaryButton label={"Cancel"} onClick={props.handleClose}></TertiaryButton>
                                  </Box>
                                  <Box>
                                    <PrimaryButton label={"Add Medication"} onClick={addMedicationHandler}></PrimaryButton>
                                  </Box>
                                </Stack>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </form>
                    )}
                    />
                  }
                </Box>
              </Box>
            </Box>
          </Container>
        </Box>
      </DialogContent>
    </>
  )
};

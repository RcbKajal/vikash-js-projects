import { Box, Container, DialogContent, DialogTitle, Grid, IconButton, Stack, Typography } from "@mui/material";
import { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from "react";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { useAppDispatch, useAppSelector } from "../../../store";

import { AddMedicationForm } from "../../../services/pages/popup-form/AddMedicationForm.service";
import { AxiosResponse } from "axios";
import CloseIcon from "@mui/icons-material/Close";
import { DialogTitleProps } from "../../../interfaces/DialogTitleProps";
import { InputComment } from "../../../core/forms/InputComment";
import { InputSelect } from "../../../core/forms/inputs/InputSelect";
import { InputText } from "../../../core/forms/inputs/InputText";
import { PRODUCT_CATALOG } from "../../../constants/Endpoints";
import PhysicianDoctor from "../../../constants/api";
import PrimaryButton from "../../../core/buttons/primary-button";
import { Product } from "../../../models/Product";
import { ProductSize } from "../../../models/ProductSize";
import TertiaryButton from "../../../core/buttons/tertiary-button";
import { productActions } from "../../../store/Actions";

function BootstrapDialogTitle(props: DialogTitleProps) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton onClick={onClose} sx={{ position: "absolute", right: 8, top: 8, color: (theme) => theme.palette.grey[500] }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export const AddPrescriptionMedication = (props: { handleClose: () => void, handleAddPrescriptionHistory: () => void }) => {

  const dispatch = useAppDispatch();
  const ProductCatalogs = useAppSelector(state => state.productReducer.products);

  const [sizes, setSizes]: [ProductSize[], Dispatch<SetStateAction<ProductSize[]>>] = useState<ProductSize[]>([]);

  useEffect(() => {
    AddMedicationForm.reset();
    getProducts();
  });

  const getProducts = async () => {
    try {
      const res: AxiosResponse = await PhysicianDoctor.post(PRODUCT_CATALOG, {}, { params: { api_key: localStorage.getItem("api_key") } });

      const productData = res.data.response;
      if (productData !== null) {
        dispatch(productActions.setProductCatalogData({ data: productData }));
      }
    } catch (err) { }
  }

  const onSelectSize = (e: ChangeEvent<HTMLInputElement>) => {
    const index = Number(e.target.value);
    AddMedicationForm.patchValue({
      quantity: sizes[index].quantity,
      supply: sizes[index].day_supply,
      refills: sizes[index].refills,
      comments: '',
    })
  }

  const onSelectMedication = (e: ChangeEvent<HTMLInputElement>) => {
    const selected = ProductCatalogs.find((item: Product) => item.product_id === e.target.value);
    setSizes(selected.Variations);
    AddMedicationForm.patchValue({
      size: '0',
      quantity: selected.Variations[0].quantity,
      supply: selected.Variations[0].day_supply,
      refills: selected.Variations[0].refills,
      choose_dosing_instruction: selected.sig_dosing_instruction,
      choose_medical_necessity: selected.medical_necessity,
      comments: '',
    })
  };


  return (
    <>
      <BootstrapDialogTitle id="customized-dialog-title" onClose={props.handleClose}></BootstrapDialogTitle>
      <DialogContent dividers className="popup_content_medication">
        <Box component="main" className="popup-info_medication">
          <Container maxWidth="lg">
            <Box className="main-box">
              <Box sx={{ bgcolor: "background.paper" }}>
                <Stack className="modal_heading_main" direction="row" justifyContent="center" alignItems="center">
                  <Typography className="heading_bottom_without_border">Add Medication</Typography>
                </Stack>
                <Box>
                  {ProductCatalogs &&
                    <FieldGroup control={AddMedicationForm} render={({ get, invalid }) => (
                      <form>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="medication" render={InputSelect} meta={{
                              name: "medication", options: ProductCatalogs.map((item: Product) => {
                                return { label: item.medication_name, value: item.product_id, data: item };
                              }), label: "Medication", placeholder: "Please Enter Medication", onChange: onSelectMedication
                            }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="size" render={InputSelect} meta={{
                              name: "size", options: sizes.map((item: { size: string }, index: number) => {
                                return { label: item.size, value: index, data: item };
                              }), label: "Size", placeholder: "Size", onChange: onSelectSize
                            }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="quantity" render={InputText} meta={{ name: "quantity", label: "Quantity", placeholder: "Please Enter Quantity" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="supply" render={InputText} meta={{ name: "supply", label: "Day Supply (Edit As Needed)", placeholder: "Please Enter Day Supply (Edit As Needed)" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="refills" render={InputText} meta={{ name: "refills", label: "Refills", placeholder: "Please Enter Refills" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} lg={6}>
                            <FieldControl name="choose_dosing_instruction" render={InputText} meta={{ name: "choose_dosing_instruction", label: "Choose Dosing Instructions", placeholder: "Please Enter Choose Dosing Instructions" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="choose_medical_necessity" render={InputText} meta={{ name: "choose_medical_necessity", label: "Choose Medical Necessity", placeholder: "Please Enter Choose Medical Necessity" }} />
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <FieldControl name="comments" render={InputComment} meta={{ name: "comments", value: "comments", label: "Comments", helperText: "Comments is required", placeholder: "Write Comments........." }} />
                          </Grid>
                          <Grid className="billing_info bottom" pt="0" container spacing={2}>
                            <Grid item xs={12} sm={12} md={12} lg={12} pl={0}>
                              <Grid item className="inputs_fields_ratio">
                                <ul>
                                  <li>
                                    <Box className="mandatory">
                                      <span> * </span> This field is mandatory
                                    </Box>
                                  </li>
                                  <li>
                                    <TertiaryButton label={"Cancel"} onClick={props.handleClose}></TertiaryButton>
                                  </li>
                                  <li>
                                    <PrimaryButton label={"Add Medication"} onClick={props.handleAddPrescriptionHistory}></PrimaryButton>
                                  </li>
                                </ul>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </form>
                    )}
                    />
                  }
                </Box>
              </Box>
            </Box>
          </Container>
        </Box>
      </DialogContent>
    </>
  )
};

import { Box, Container, DialogContent, DialogTitle, Grid, IconButton, Stack, Typography } from "@mui/material";
import { CreateNewPatientForm, CreatePatientHandler } from "../../../services/pages/popup-form/CreateNewPatientForm.service";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { RootState, useAppSelector } from "../../../store";
import { cityOptions, genderOptions, stateOptions } from "../../../services/components/selectOptions.service";
import { useCallback, useEffect, useState } from "react";

import { AxiosResponse } from "axios";
import CloseIcon from "@mui/icons-material/Close";
import { DialogTitleProps } from "../../../interfaces/DialogTitleProps";
import { GET_ALLERGIES } from "../../../constants/Endpoints";
import { GoogleAutoCompleteInput } from "../../../core/forms/inputs/GoogleAutoCompleteInput";
import { InputAddress } from "../../../core/forms/inputs/InputAddress";
import { InputDate } from "../../../core/forms/inputDate";
import { InputEmail } from "../../../core/forms/InputEmail";
import { InputMultiSelect } from "../../../core/forms/inputs/InputMultiSelect";
import { InputNumber } from "../../../core/forms/inputs/InputNumber";
import { InputSelect } from "../../../core/forms/inputs/InputSelect";
import { InputText } from "../../../core/forms/inputs/InputText";
import PhysicianDoctor from "../../../constants/api";
import PrimaryButton from "../../../core/buttons/primary-button";
import { RadioInput } from "../../../core/forms/inputs/RadioInput";
import SecondaryButton from "../../../core/buttons/secondary-button";

function BootstrapDialogTitle(props: DialogTitleProps) {

  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children} {onClose ? (
        <IconButton onClick={onClose} sx={{ position: "absolute", right: 8, top: 8, color: (theme) => theme.palette.grey[500] }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export const AddNewPatient = (props: { handleClose: () => void }) => {

  const user = useAppSelector((state: RootState) => state.profileReducer.user);
  const [allergyOptions, setAllergyOptions] = useState([]);

  const getAllergies = async () => {
    try {
      const res: AxiosResponse = await PhysicianDoctor.post(GET_ALLERGIES, {}, { params: { api_key: localStorage.getItem('api_key'), constant_id: 1 } });
      setAllergyOptions(res.data.response[0].value.map((item: string) => {
        return { label: item, value: item }
      }));
    } catch(err: unknown) {
      console.error("Something went wrong");
    };
  };

  useEffect(() => {
    getAllergies();
  },[]);


  const onSelectAllergies = (data: {value: string}[]) => {
      const value = data.map((item: {value: string}) => item.value).join();
      CreateNewPatientForm.patchValue({
        allergies: value
      })
  };

  const createPatientHandler = () => {
    CreatePatientHandler(user, props);
  };

  const handleAutoCompleteChange = ({ address1, locality, short_name, postcode, country }: {
    address1: string, locality: string, short_name: string, postcode: string, country: string
  }) => {
    CreateNewPatientForm.patchValue({
      street: address1,
      city: locality,
      state: short_name,
      code: postcode,
      country: country,
    });
  };


  return (
    <>
      <BootstrapDialogTitle id="customized-dialog-title" onClose={props.handleClose}></BootstrapDialogTitle>
      <DialogContent dividers className="create_patient_content">
        {allergyOptions.length > 0 &&
          <FieldGroup control={CreateNewPatientForm} render={({ get, invalid }) => (
            <form>
              <Box component="main" className="create_patient_info">
                <Container maxWidth="lg">
                  <Box className="main-box">
                    <Box sx={{ bgcolor: "background.paper" }}>
                      <Stack className="modal_heading_main" direction="row" justifyContent="center" alignItems="center">
                        <Typography className="heading_bottom">
                          Create New Patient
                        </Typography>
                      </Stack>
                      <Box>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} md={4} lg={4}>
                            <Typography className="field-info-details-desc">
                              Personal Info
                              <span>Provide your personal info</span>
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={8}>
                            <Grid container spacing={2}>
                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="firstName" render={InputText} meta={{ name: "firstName", value: "firstName", helperText: "Patient First Name is required", label: "Patient First Name", placeholder: "Please Enter Patient First Name" }} />
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="lastName" render={InputText} meta={{ name: "lastName", value: "lastName", helperText: "Patient Last Name is required", label: "Patient Last Name", placeholder: "Please Enter Patient Last Name" }} />
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="gender" render={InputSelect} meta={{ name: "gender", value: "gender", options: genderOptions, label: "Gender", placeholder: "Please Enter Gender" }} />
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="dob" render={InputDate} meta={{ name: "dob", value: "dob", label: "Date of Birth", placeholder: "Please Enter Date of Birth" }} />
                              </Grid>

                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="email" render={InputEmail} meta={{ name: "email", value: "email", label: "Email", placeholder: "Please Enter email" }} />
                              </Grid>
                              <Grid item xs={12} sm={12} md={12} lg={6}>
                                <FieldControl name="phone" render={InputNumber} meta={{ name: "phone", value: "phone", label: "Mobile Phone Number", placeholder: "Please Enter Mobile Phone Number" }} />
                                <Typography component="p">
                                  *Entering a mobile phone number will allow us
                                  to reach the patient directly for payment.
                                </Typography>
                              </Grid>

                              <Grid item xs={12} sm={12} md={4} lg={4} className="allergies choose_allergies">
                                <Typography component="p">
                                  Patient allergies? <span>*</span>
                                </Typography>
                              </Grid>
                              <Grid item xs={12} sm={12} md={8} lg={8}>
                                <FieldControl name="isAllergies" render={RadioInput} meta={{ name: "isAllergies", defaultValue: get("isAllergies").value, options: [{ label: "Yes", value: true }, { label: "No", value: false }] }} />
                              </Grid>

                              {get("isAllergies") && get("isAllergies").value === "true" && (
                                <>
                                  <Grid item xs={12} sm={12} md={12} lg={12} pt={0}>
                                    <FieldControl name="allergies" render={InputMultiSelect} meta={{ value: "allergies", name: "allergies", label: "Allergies", placeholder: "Please Enter Allergies", options: allergyOptions, onChange:onSelectAllergies }} />
                                  </Grid>
                                </>
                              )}
                            </Grid>
                          </Grid>
                        </Grid>
                        <Box className="divider-horizontal"></Box>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} md={4} lg={4}>
                            <Typography className="field-info-details-desc">
                              Patient Address
                              <span>Please provide patient’s address</span>
                            </Typography>
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={8}>
                            <Grid container spacing={2}>
                              <Grid item xs={12} md={12} lg={12} className="google-auto-complete-container" style={{ zIndex: 9999 }}>
                                <GoogleAutoCompleteInput uniqueKey={"newPatient-auto-complete"} handleAutoCompleteChange={handleAutoCompleteChange} />
                              </Grid>

                              <Grid item xs={12} md={6} lg={6}>
                                <FieldControl name="apt_suite" render={InputAddress} meta={{ name: "apt_suite", helperText: "Apt./Suite  is required", label: "Apt./Suite", placeholder: "Please Enter Apt./Suite", required: false }} />
                              </Grid>
                              <Grid item xs={12} md={6} lg={6}>
                                <FieldControl name="city" render={InputAddress} meta={{ name: "city", options: cityOptions, label: "City", placeholder: "Please Enter City" }} />
                              </Grid>

                              <Grid item xs={12} md={6} lg={6}>
                                <FieldControl name="state" render={InputAddress} meta={{ name: "state", options: stateOptions, label: "State", placeholder: "Please Enter State" }}
                                />
                              </Grid>
                              <Grid item xs={12} md={6} lg={6}>
                                <FieldControl name="code" render={InputAddress} meta={{ name: "code", helperText: "Postal Code is required", label: "Postal Code", placeholder: "Please Enter Postal Code" }}
                                />
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Box className="divider-horizontal"></Box>
                        <Grid item container spacing={2}>
                          <Grid item xs={12} sm={12} md={4} lg={4}>
                            <Typography className="field-info-details-desc">
                              Billing Information
                              <span>
                                Please provide patient billing information
                              </span>
                            </Typography>
                          </Grid>
                          <Grid className="billing_info" item xs={12} sm={12} md={12} lg={8}>
                            <Grid item container spacing={2} className="gridTop">
                              <Grid item xs={12} sm={12} md={6} lg={5} pr={4} className="allergies choose_allergies">
                                <Typography component="p" pr={4}>
                                  Add a payment method? <span>*</span>
                                </Typography>
                              </Grid>
                              <Grid item xs={12} sm={12} md={6} lg={7}>
                                <FieldControl name="isPaymentMethod" render={RadioInput} meta={{ name: "isPaymentMethod", defaultValue: get("isPaymentMethod").value, options: [{ label: "Yes", value: true }, { label: "No", value: false }] }} />
                              </Grid>
                              <Stack className="inputs_fields_ratio" direction="row" ml={3}>
                                <Typography component="h5">
                                  OPTIONAL. If you do not enter a payment method, we will contact <br></br> your patient by phone for payment.
                                </Typography>
                              </Stack>
                              {get("isPaymentMethod") &&
                                get("isPaymentMethod").value === "true" && (
                                  <>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                      <Box className="credit_card">
                                        <Typography>Add Credit Card</Typography>
                                      </Box>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={6} lg={6}>
                                      <FieldControl name="cardholderName" render={InputText} meta={{ name: "cardholderName", value: "cardholderName", label: "Cardholder Name", placeholder: "Please Enter Cardholder Name" }} />
                                    </Grid>

                                    <Grid item xs={12} sm={12} md={6} lg={6}>
                                      <FieldControl name="cardNumber" render={InputText} meta={{ name: "cardNumber", value: "cardNumber", label: "Card Number", placeholder: "Please Enter Card Number" }} />
                                    </Grid>

                                    <Grid item xs={12} sm={12} md={6} lg={6}>
                                      <FieldControl name="expiry" render={InputText} meta={{ name: "expiry", value: "expiry", label: "Expiry Date", placeholder: "Please Enter Expiry Date" }} />
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={6} lg={6}>
                                      <FieldControl name="cvv" render={InputText} meta={{ name: "cvv", value: "cvv", label: "CVC/CVV", placeholder: "Please Enter CVC/CVV" }} />
                                    </Grid>

                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                      <FieldControl name="zip_code" render={InputText} meta={{ name: "zip_code", value: "zip_code", label: "Billing Zip Code", placeholder: "Please Enter Billing Zip Code" }} />
                                    </Grid>
                                  </>
                                )}
                              <Grid item xs={12} sm={12} md={12} lg={12} pl={0}>
                                <Stack className="d-vh-between" direction="row" mt={3}>
                                  <Box className="mandatory">
                                    <span>*</span> This field is mandatory
                                  </Box>
                                  <Box className="d-vh-between" gap={1} mt={1}>
                                    <Box style={{ width: "160px" }}>
                                      <SecondaryButton label="Cancel" onClick={props.handleClose}></SecondaryButton>
                                    </Box>
                                    <Box style={{ width: "160px" }}>
                                      <PrimaryButton label="Create Patient" onClick={createPatientHandler}></PrimaryButton>
                                    </Box>
                                  </Box>
                                </Stack>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Box>
                    </Box>
                  </Box>
                </Container>
              </Box>
            </form>
          )}
          />}
      </DialogContent>
    </>
  );
};

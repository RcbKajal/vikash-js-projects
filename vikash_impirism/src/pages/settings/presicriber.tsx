import { Box, Button, Container, Dialog, FormControlLabel, Grid, Radio, RadioGroup, Stack, Typography, styled } from "@mui/material";
import { ChangeEvent, MouseEventHandler, useEffect, useState } from "react";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { SettingPrescriberHandler, prescriberForm } from "../../services/pages/settings/PrescriberForm";
import { useAppDispatch, useAppSelector } from "../../store";

import { AddNewAddress } from "../../components/popup/AddNewAddress";
import { AddNewCard } from "../../components/popup/AddNewCard";
import { AddPrescriber } from "../../components/popup/AddPrescriber";
import { AxiosResponse } from "axios";
import Billing from "../../assets/icons/billing.svg";
import { EditShippingAddress } from "../../components/popup/EditShippingAddress";
import Home from "../../assets/icons/shipping_home.svg";
import { InputEmail } from "../../core/forms/InputEmail";
import { InputNumber } from "../../core/forms/inputs/InputNumber";
import { InputSelect } from "../../core/forms/inputs/InputSelect";
import { InputText } from "../../core/forms/inputs/InputText";
import { PRESCRIBER_PROFILE_GET } from "../../constants/Endpoints";
import PhysicianDoctor from "../../constants/api";
import Setting_plus from "../../assets/icons/setting_plus.svg";
import { User } from "../../models/User";
import UserBlack from "../../assets/icons/user_black.svg";
import UserImage from "../../assets/icons/user.svg";
import User_profile from "../../assets/icons/profile_user.svg";
import cameraPic from '../../assets/icons/camera.svg';
import deleteIcon from "../../assets/icons/deleteIcon.svg";
import editIcon from "../../assets/icons/editIcon.svg";
import { notificationOptions } from "../../services/components/selectOptions.service";
import { settingsActions } from "../../store/Actions";

export const Prescriber = () => {

    const userData = useAppSelector(state => state.profileReducer.user);
    const specialties = useAppSelector(state => state.physicanReducer.dropdownData);
    const [selectedPrescriberIndex, setSelectedPrescriberIndex] = useState(0);
    const prescribers: User[] = useAppSelector(state => state.settingReducer.prescribers);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (prescribers) {
            prescriberForm.patchValue({
                firstName: prescribers[selectedPrescriberIndex].first_name,
                lastName: prescribers[selectedPrescriberIndex].last_name,
                phone: prescribers[selectedPrescriberIndex].mobile,
                email: prescribers[selectedPrescriberIndex].email,
                notification_preference: prescribers[selectedPrescriberIndex].notification,
                npi: prescribers[selectedPrescriberIndex].npi,
                specialty: prescribers[selectedPrescriberIndex].Specialty,
            });
        }
    }, [selectedPrescriberIndex, prescribers]);

    const [data, setData] = useState({
        card: false,
        prescriber: false,
        address: false,
        edit: false,
        addPrescriber: false,
    });

    const SettingPrescriberInfoHandler: MouseEventHandler<HTMLButtonElement> = (e) => {
        SettingPrescriberHandler(userData);
    };

    const handleClick = (index: number) => {
        setSelectedPrescriberIndex(index);
    };

    const handleNewCard = () => {
        setData((oldData) => ({
            ...oldData,
            card: true,
        }));
    };

    const handleAddPrescriber = () => {
        setData((oldData) => ({
            ...oldData,
            addPrescriber: true,
        }));
    };

    const handleNewAddress = () => {
        setData((oldData) => ({
            ...oldData,
            address: true,
        }));
    };

    const handleEditAddress = () => {
        setData((oldData) => ({
            ...oldData,
            edit: true,
        }));
    };


    const loadPrescriber = async () => {
        try {
            const res: AxiosResponse = await PhysicianDoctor.post(PRESCRIBER_PROFILE_GET, {}, { params: { api_key: localStorage.getItem('api_key'), staff_id: userData.doctor_id } })
            dispatch(settingsActions.setPrescriptionData({ data: User.createFromArray(res.data.response) as User[] }));
        } catch (err) {
            console.error("Something went wrong");
        }
    }


    const handleImageChange = async (e: ChangeEvent<HTMLInputElement>, user: User) => {
        if (e?.target?.files?.length) {
            try {
                e.preventDefault();
                const formData = new FormData();
                formData.append("profilepic", e.target.files[0]);
                formData.append("doctor_id", user.doctor_id);
                formData.append("api_key", (localStorage.getItem('api_key') as string));

                // PhysicianDoctor.post(`https://mobileauth.imprimisrx.com/development/webservices/1.0.8/index.php/doctorstaffapi/updateprofilepic`,
                await PhysicianDoctor.post(`https://mobileauth.imprimisrx.com/development/webservices/1.0.8/index.php/doctorapi/updateprofilepic`,
                    formData,
                    {
                        method: "POST",
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });
                loadPrescriber();
            } catch (err) {
                console.log(err);
            };
        }
    }

    const handleClose = (e?: string | React.SyntheticEvent) => {
        if (e === 'New Prescriber Added') {
            loadPrescriber();
        }
        setData((oldData) => ({
            ...oldData,
            card: false,
            prescriber: false,
            address: false,
            edit: false,
            addPrescriber: false,
        }));
    };

    const BootstrapDialog = styled(Dialog)(({ theme }) => ({
        "& .MuiDialogContent-root": {
            padding: theme.spacing(2),
        },
        "& .MuiDialogActions-root": {
            padding: theme.spacing(1),
        },
    }));


    return (
        <>
            {data.card && (
                <BootstrapDialog onClose={() => handleClose()} open={data.card} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
                    <AddNewCard handleClose={handleClose} />
                </BootstrapDialog>
            )}
            {data.address && (
                <BootstrapDialog onClose={() => handleClose()} open={data.address} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
                    <AddNewAddress handleClose={handleClose} />
                </BootstrapDialog>
            )}
            {data.edit && (
                <BootstrapDialog onClose={() => handleClose()} open={data.edit} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
                    <EditShippingAddress handleClose={handleClose} />
                </BootstrapDialog>
            )}
            {data.addPrescriber && (
                <BootstrapDialog onClose={() => handleClose()} open={data.addPrescriber} PaperProps={{ style: { minHeight: "60%", maxHeight: "89%", minWidth: " 40%", maxWidth: 650 } }}>
                    <AddPrescriber handleClose={handleClose} />
                </BootstrapDialog>
            )}

            <Box className="payment">
                <Container maxWidth="xl">
                    <Stack direction="row" alignItems="center" marginTop={{ xs: 0, sm: 1, md: 2 }}>
                        <Grid item container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                            <Grid item xs={12} sm={12} md={12} lg={3}>
                                <Stack direction="row" className="setting_profile">
                                    <Box className="setting_name" mt={3} pt={3}>
                                        <ul>
                                            <li>
                                                <Typography component="h3">Prescribers</Typography>
                                            </li>
                                            {
                                                prescribers && prescribers.map((prescriber: User, index: number) => {
                                                    return (
                                                        <li key={index}>
                                                            <Button className={index === selectedPrescriberIndex ? "active profile_prescriber_btn" : "profile_prescriber_btn"} id={"1"} onClick={() => handleClick(index)} variant="outlined">
                                                                <div className="search-inputfield-btn">{index === selectedPrescriberIndex ? <img src={User_profile} alt="search" height={18} width={20} /> : <img src={UserBlack} alt="search" height={18} width={20} />}</div>
                                                                {prescriber.first_name}
                                                            </Button>
                                                        </li>)
                                                })
                                            }
                                            <li>
                                                <Button className="edit_btn settings_btn_last" variant="outlined" onClick={handleAddPrescriber} style={{ color: "#00ACBA", fontSize: "22px", fontWeight: "600", backgroundColor: "#fff", border: "1px solid #fff", borderRadius: "8px", boxShadow: "none", height: "45px", width: "230px", textAlign: "left", textTransform: "capitalize", marginTop: "5px", display: "flex", alignItems: "center", justifyContent: "flex-start", paddingLeft: "0px" }}>
                                                    <div className="search-inputfield-btn">
                                                        <img src={Setting_plus} alt="search" height={22} width={22} />
                                                    </div>
                                                    Add Prescriber
                                                </Button>
                                            </li>
                                        </ul>
                                    </Box>
                                </Stack>
                            </Grid>

                            {prescribers && prescribers.length > 0 && (
                                <>
                                    <Grid item xs={12} sm={12} md={12} lg={2}>
                                        <Stack direction="row" className="setting_profile">
                                            <Stack width={{ lg: "100%" }} mt={4} pt={4} className="girlAlignment">
                                                <Box className="card-user-profile-img" style={(prescribers[selectedPrescriberIndex].profile_image_path) ? { background: `url('https://mobileauth.imprimisrx.com/development/webservices/images/originals/${prescribers[selectedPrescriberIndex].profile_image_path}')` } : {}}>
                                                    <label className="camera" htmlFor="upload-button">
                                                        <img src={cameraPic} alt='camera' width={38} height={38} />
                                                    </label>
                                                    <input type="file" id="upload-button" style={{ display: "none" }} onChange={(e) => handleImageChange(e, prescribers[selectedPrescriberIndex])} />
                                                </Box>

                                            </Stack>
                                        </Stack>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={7}>
                                        <Box className="personal_details personal_details_block">
                                            <Typography className="heading">
                                                <span className="profile_icon">
                                                    <img src={UserImage} alt="Imprimis RX A Harrow Company" width={16} />
                                                </span>
                                                Personal
                                            </Typography>
                                        </Box>
                                        <FieldGroup control={prescriberForm} render={({ get, invalid }) => (
                                            <form>
                                                <Stack className="setting_info" gap={{ xs: 2.25, sm: 2.25, md: 2.25, lg: 2.25 }}>
                                                    <Stack className="inputs_fields_ratio" direction="row">
                                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                                            <Stack className="inputs_fields_ratio" direction="row" gap={{ xs: 1.5, sm: 1.5, md: 1.5, lg: 1.5 }}>
                                                                <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                    <FieldControl name="firstName" render={InputText} meta={{ name: "firstName", value: "firstName", helperText: "First Name is required", label: "Prescriber First Name", placeholder: "Please Enter Prescriber First Name" }} />
                                                                </Grid>
                                                                <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                    <FieldControl name="lastName" render={InputText} meta={{ name: "lastName", value: "lastName", helperText: "Last Name is required", label: "Prescriber Last Name", placeholder: "Please Enter Prescriber Last Name" }} />
                                                                </Grid>
                                                            </Stack>
                                                        </Grid>
                                                    </Stack>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Stack className="inputs_fields_ratio" direction="row" gap={{ xs: 1.5, sm: 1.5, md: 1.5, lg: 1.5 }}>
                                                            <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                <FieldControl name="phone" render={InputNumber} meta={{ name: "phone", value: "phone", helperText: "Phone is required", label: "Phone", placeholder: "Please Enter Phone Number" }} />
                                                            </Grid>
                                                            <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                <FieldControl name="notification_preference" render={InputSelect} meta={{ name: "notification_preference", value: "notification_preference", options: notificationOptions, label: "Notification Preference", placeholder: "Please Enter Notification Preference" }} />
                                                            </Grid>
                                                        </Stack>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Stack className="inputs_fields_ratio" direction="row" gap={{ xs: 1.5, sm: 1.5, md: 1.5, lg: 1.5 }}>
                                                            <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                <FieldControl name="email" render={InputEmail} meta={{ name: "email", value: "email", helperText: "Email is Required.", label: "Email", placeholder: "Please Enter Email" }} />
                                                            </Grid>
                                                            <Grid item xs={12} sm={12} md={6} lg={6}>
                                                                <FieldControl name="npi" render={InputText} meta={{ name: "npi", value: "npi", helperText: "NPI is required", label: "NPI", placeholder: "Please Enter NPI" }} />
                                                            </Grid>
                                                        </Stack>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <Stack className="inputs_fields_ratio" direction="row" gap={{ xs: 1.5, sm: 1.5, md: 1.5, lg: 1.5 }}>
                                                            <Grid item xs={12} sm={12} md={12} lg={9}>
                                                                <FieldControl name="speciality" render={InputSelect} meta={{ name: "speciality", value: "speciality", options: specialties, label: "Specialty", placeholder: "Please Enter Specialty", helperText: "Specialty is required" }} />
                                                            </Grid>
                                                            <Grid item xs={12} sm={12} md={12} lg={3}>
                                                                <Stack alignItems="center" justifyContent="center">
                                                                    <Box className="add_new_cart" alignItems="center" justifyContent="flex-end">
                                                                        <button
                                                                            className="edit_btn"
                                                                            style={{
                                                                                color: "#00ACBA",
                                                                                fontSize: "20px",
                                                                                fontWeight: "700",
                                                                                backgroundColor: "#fff",
                                                                                border: "2px solid #00ACBA",
                                                                                borderRadius: "8px",
                                                                                boxShadow: "none",
                                                                                height: "46px",
                                                                                width: "198px",
                                                                                textTransform: "capitalize",
                                                                            }}
                                                                            onClick={SettingPrescriberInfoHandler}
                                                                        >
                                                                            Save
                                                                        </button>
                                                                    </Box>
                                                                    {/* <Box className="add_new_cart" alignItems="center" justifyContent="flex-end">
                                                                            <Button className="edit_btn" variant="outlined" onClick={SettingPrescriberInfoHandler} style={{ color: "#93999C", fontSize: "19px", fontWeight: "600", backgroundColor: "rgba(38, 50, 56, 0.1)", border: "none", borderRadius: "8px", boxShadow: "none", height: "56px", width: "145px", textTransform: "capitalize" }}>
                                                                                Save
                                                                            </Button>
                                                                        </Box> */}
                                                                </Stack>
                                                            </Grid>
                                                        </Stack>
                                                    </Grid>
                                                </Stack>
                                            </form>
                                        )}
                                        />
                                        <Stack className="setting_billing" direction="row" justifyContent="space-around" gap={5} mb={4} mt={4}>
                                            <Grid item container xs={12} sm={12} md={6} lg={6}>
                                                <Box className="exeasting gap_top personal_details">
                                                    <Typography className="heading">
                                                        <span className="profile_icon">
                                                            <img src={Billing} alt="Imprimis RX A Harrow Company" width={16} />
                                                        </span>
                                                        BILLING INFO
                                                    </Typography>
                                                    <Typography component="h2" mt={3}>
                                                        Existing Cards
                                                    </Typography>

                                                    <RadioGroup defaultValue="female" className="radio_grid" aria-labelledby="demo-customized-radios" name="customized-radios">
                                                        <Stack direction="row" alignItems="center">
                                                            <FormControlLabel value="female" control={<Radio />} label="American Express - x4458" />
                                                            <Stack>
                                                                <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                                            </Stack>
                                                        </Stack>
                                                        <Stack direction="row" alignItems="center">
                                                            <FormControlLabel value="male" control={<Radio />} label="Mastercard - x8597" />
                                                            <Stack>
                                                                <img src={deleteIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                                            </Stack>
                                                        </Stack>
                                                    </RadioGroup>

                                                </Box>
                                                <Box className="add_new_cart">
                                                    <button className="edit_btn" style={{ color: "#00ACBA", fontSize: "20px", fontWeight: "700", backgroundColor: "#fff", border: "2px solid #00ACBA", borderRadius: "8px", boxShadow: "none", height: "46px", width: "198px", textTransform: "capitalize" }} onClick={handleNewCard}>
                                                        Add New Card
                                                    </button>
                                                </Box>
                                            </Grid>
                                            <Grid item container xs={12} sm={12} md={6} lg={6}>
                                                <Box className="exeasting gap_top personal_details">
                                                    <Typography className="heading">
                                                        <span className="profile_icon">
                                                            <img src={Home} alt="Imprimis RX A Harrow Company" width={16} />
                                                        </span>
                                                        SHIPPING INFO
                                                    </Typography>
                                                    <Typography component="h2" mt={3}>
                                                        Shipping Addresses
                                                    </Typography>

                                                    <RadioGroup defaultValue="female" className="radio_grid" aria-labelledby="demo-customized-radios" name="customized-radios">
                                                        <Stack direction="row" alignItems="center">
                                                            <FormControlLabel value="female" control={<Radio />} label="Joseph Nashville" />
                                                            <Stack onClick={handleEditAddress}>
                                                                <img src={editIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                                            </Stack>
                                                        </Stack>
                                                        <Stack direction="row" alignItems="center">
                                                            <FormControlLabel value="male" control={<Radio />} label="Mary Kentucky" />
                                                            <Stack onClick={handleEditAddress}>
                                                                <img src={editIcon} alt="Imprimis RX A Harrow Company" width={16} />
                                                            </Stack>
                                                        </Stack>
                                                    </RadioGroup>
                                                </Box>
                                                <Box className="add_new_cart">
                                                    <button
                                                        className="edit_btn"
                                                        style={{
                                                            color: "#00ACBA",
                                                            fontSize: "20px",
                                                            fontWeight: "700",
                                                            backgroundColor: "#fff",
                                                            border: "2px solid #00ACBA",
                                                            borderRadius: "8px",
                                                            boxShadow: "none",
                                                            height: "46px",
                                                            width: "198px",
                                                            textTransform: "capitalize",
                                                        }}
                                                        onClick={handleNewAddress}
                                                    >
                                                        Add New Address
                                                    </button>
                                                </Box>
                                            </Grid>
                                        </Stack>
                                    </Grid>
                                </>
                            )}

                        </Grid>
                    </Stack>
                </Container>
            </Box>
        </>)
}
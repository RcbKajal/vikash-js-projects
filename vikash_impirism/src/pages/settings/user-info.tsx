import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material";
import { FieldControl, FieldGroup } from "react-reactive-form";
import { RootState, useAppDispatch, useAppSelector } from "../../store";
import { SettingLoginHandler, SettingUserHandler } from "../../services/pages/settings/personalLoginForm";
import { personalLoginForm, personalUserForm } from "../../services/pages/settings/personalLoginForm";

import { AxiosResponse } from "axios";
import { ChangeEvent } from "react";
import EmailInputIcon from "../../core/forms/inputs/EmailInputIcon";
import { InputNumber } from "../../core/forms/inputs/InputNumber";
import { InputText } from "../../core/forms/inputs/InputText";
import Login from "../../assets/icons/login.svg";
import PasswordInput from "../../core/forms/inputs/PasswordInputIcon";
import PhysicianDoctor from "../../constants/api";
import { User } from "../../models/User";
import cameraPic from '../../assets/icons/camera.svg';
import { profileActions } from "../../store/Actions";
import profilePic from "../../assets/icons/user.svg";

export const UserInfo = () => {

    const dispatch = useAppDispatch();

    const handleImageChange = async (e: ChangeEvent<HTMLInputElement>) => {
        if (e?.target?.files?.length) {

            try {
                e.preventDefault();
                const formData = new FormData();
                formData.append("profilepic", e.target.files[0]);

                const result = await PhysicianDoctor.post(`https://mobileauth.imprimisrx.com/development/webservices/1.0.7/index.php/doctorapi/updateprofilepic?api_key=${localStorage.getItem('api_key')}&doctor_id=${user.doctor_id}`,
                    formData,
                    {
                        method: "POST",
                        headers: {
                            "Content-Type": "multipart/form-data",
                        }
                    });

                const duplicateUser = { ...user };
                duplicateUser.profile_image_path = result.data.response.profile_image_path;
                dispatch(profileActions.setUserData({ user: duplicateUser as User }))

            } catch (err) {
                console.log(err);
            }
        }
    };

    const user: User = useAppSelector((state: RootState) => state.profileReducer.user);

    const SettingLoginInfoHandler = () => {
        SettingLoginHandler(user);
    };

    const SettingPersonalInfoHandler = () => {
        SettingUserHandler(user);
    };

    return (
        <Box>
            <Container maxWidth="xl">
                <Stack direction="row" alignItems="center">
                    <Grid item container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} padding={{ xs: 0, sm: 0, md: 0 }}>
                        <Grid item xs={12} sm={12} md={12} lg={2} padding={{ xs: 0, sm: 0, md: 0 }}>
                            <Box>
                                <Typography className="profile" style={{ padding: '28px 0px' }}>
                                    <span>Profile</span>
                                </Typography>
                                <Box className="card-user-profile-img" style={(user?.profile_image_path) ? { background: `url('https://mobileauth.imprimisrx.com/development/webservices/images/originals/${user?.profile_image_path}')` } : {}}>
                                    <label className="camera" htmlFor="upload-button">
                                        <img src={cameraPic} alt='camera' width={38} height={38} />
                                    </label>
                                    <input type="file" id="upload-button" style={{ display: "none" }} onChange={handleImageChange} />
                                </Box>
                            </Box>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={10} padding={{ xs: 0, sm: 0, md: 0 }} mt={12} pt={12}>
                            <Stack className="setting_info" direction="row" gap={5} mb={4}>
                                <Grid item xs={12} sm={12} md={12} lg={6}>
                                    <Box className="personal_details">
                                        <Typography className="heading">
                                            <span className="profile_icon">
                                                <img src={profilePic} alt="image" width={16} />
                                            </span>
                                            Personal
                                        </Typography>
                                        <FieldGroup control={personalUserForm} render={({ get, invalid }) => (
                                            <form>
                                                <Box className="personal personal_set_top">
                                                    <FieldControl name="firstName" render={InputText} meta={{ label: "First Name", name: "firstName", value: "firstName", helperText: "First Name is required", placeholder: "Please Enter First Name" }} />
                                                </Box>
                                                <Box className="personal">
                                                    <FieldControl name="lastName" render={InputText} meta={{ name: "lastName", value: "lastName", helperText: "Last Name is required", label: "Last Name", placeholder: "Please Enter Last Name" }} />
                                                </Box>
                                                <Box className="personal">
                                                    <FieldControl name="phone" render={InputNumber} meta={{ name: "phone", value: "phone", helperText: "Phone is required", label: "Phone", placeholder: "Please Enter Phone" }} />
                                                </Box>
                                                <Box className="personal">
                                                    <FieldControl name="email" render={InputText} meta={{ name: "email", value: "email", helperText: "Email is required", label: "Email", placeholder: "Please Enter Email" }} />
                                                </Box>
                                            </form>
                                        )}
                                        />

                                        <Box className="personal">
                                            <Button className="edit_btn" variant="outlined" style={{ color: "#00ACBA", fontSize: "16px", fontWeight: "600", backgroundColor: "#fff", border: "1px solid #00ACBA", borderRadius: "8px", boxShadow: "none", height: "45px", width: "135px", textTransform: "capitalize" }} onClick={SettingPersonalInfoHandler}>
                                                Save
                                            </Button>
                                        </Box>
                                    </Box>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12} lg={6}>
                                    <Box className="login_details">
                                        <Typography className="heading">
                                            <span className="profile_icon">
                                                <img src={Login} alt="image" width={16} />
                                            </span>
                                            Login
                                        </Typography>
                                        <FieldGroup control={personalLoginForm} render={({ get, invalid }) => (
                                            <form>
                                                <Box className="personal personal_set_top">
                                                    <FieldControl name="username" render={EmailInputIcon} meta={{ name: "username", value: "username", helperText: "Username is required", label: "Username", placeholder: "Please Enter Username" }} />
                                                </Box>
                                                <Box className="personal">
                                                    <FieldControl name="password" render={PasswordInput} meta={{ name: "password", value: "password", helperText: "Password is required", label: "Password", placeholder: "Please Enter Password" }} />
                                                </Box>
                                            </form>
                                        )}
                                        />
                                        <Box className="personal">
                                            <Button className="edit_btn" variant="outlined" onClick={SettingLoginInfoHandler} style={{ color: "#00ACBA", fontSize: "16px", fontWeight: "600", backgroundColor: "#fff", border: "1px solid #00ACBA", borderRadius: "8px", boxShadow: "none", height: "45px", width: "135px", textTransform: "capitalize" }}>
                                                Save
                                            </Button>
                                        </Box>
                                    </Box>
                                </Grid>
                            </Stack>
                        </Grid>
                    </Grid>
                </Stack>
            </Container>
        </Box>)
}
